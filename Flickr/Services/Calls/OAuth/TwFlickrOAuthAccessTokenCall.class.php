<?php

class TwFlickrOAuthAccessTokenCall extends TwFlickrOAuthServiceCall
{
    public function __construct()
    {
        $this->allowArgument('oauth_nonce')
            ->allowArgument('oauth_timestamp')
            ->allowArgument('oauth_verifier')
            ->allowArgument('oauth_signature_method', 'HMAC-SHA1')
            ->allowArgument('oauth_version', '1.0')
            ->allowArgument('oauth_token')
            ->allowArgument('oauth_signature')
            ->allowArgument('oauth_consumer_key')
            ->allowArgument('oauth_token_secret')
            ->setServiceMethod('access_token')
            ->setHttpMethod('GET');
    }

    public function setOAuthNonce($oAuthNonce)
    {
        $this->setArgument('oauth_nonce', $oAuthNonce);

        return $this;
    }

    public function setOAuthTimeStamp($oAuthTimeStamp)
    {
        $this->setArgument('oauth_timestamp', $oAuthTimeStamp);

        return $this;
    }

    public function setOAuthConsumerKey($oAuthConsumerKey)
    {
        $this->setArgument('oauth_consumer_key', $oAuthConsumerKey);

        return $this;
    }

    public function setOAuthSignature($oAuthSignature)
    {
        $this->setArgument('oauth_signature', $oAuthSignature);

        return $this;
    }

    public function setOAuthSignatureMethod($oAuthSignatureMethod)
    {
        $this->setArgument('oauth_signature_method', $oAuthSignatureMethod);

        return $this;
    }

    public function setOAuthVersion($oAuthVersion)
    {
        $this->setArgument('oauth_version', $oAuthVersion);

        return $this;
    }

    public function setOAuthVerifier($oAuthVerifier)
    {
        $this->setArgument('oauth_verifier', $oAuthVerifier);

        return $this;
    }


    public function setOAuthToken($oAuthToken)
    {
        $this->setArgument('oauth_token', $oAuthToken);

        return $this;
    }

    public function setOAuthTokenSecret($oAuthTokenSecret)
    {
        $this->setArgument('oauth_token_secret', $oAuthTokenSecret);

        return $this;
    }
}