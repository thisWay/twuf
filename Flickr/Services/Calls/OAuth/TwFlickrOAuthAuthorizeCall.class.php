<?php

class TwFlickrOAuthAuthorizeCall extends TwFlickrOAuthServiceCall
{
    public function __construct()
    {
        $this->allowArgument('perms')
            ->allowArgument('oauth_token')
            ->setServiceMethod('authorize');
    }

    public function setOAuthToken($token)
    {
        $this->setArgument('oauth_token', $token);

        return $this;
    }

    public function setPerms($perms)
    {
        $this->setArgument('perms', $perms);

        return $this;
    }
}