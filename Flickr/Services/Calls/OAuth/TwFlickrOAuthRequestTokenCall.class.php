<?php

class TwFlickrOAuthRequestTokenCall extends TwFlickrOAuthServiceCall
{
    public function __construct()
    {
        parent::__construct();

        $this->setArgument('method', 'request_token')
            ->allowArgument('oauth_callback')
            ->setHttpMethod('GET');
    }

    public function setOAuthNonce($oAuthNonce)
    {
        $this->setArgument('oauth_nonce', $oAuthNonce);

        return $this;
    }

    public function setOAuthTimeStamp($oAuthTimeStamp)
    {
        $this->setArgument('oauth_timestamp', $oAuthTimeStamp);

        return $this;
    }

    public function setOAuthConsumerKey($oAuthConsumerKey)
    {
        $this->setArgument('oauth_consumer_key', $oAuthConsumerKey);

        return $this;
    }

    public function setOAuthSignature($oAuthSignature)
    {
        $this->setArgument('oauth_signature', $oAuthSignature);

        return $this;
    }

    public function setOAuthCallback($oAuthCallback)
    {
        $this->setArgument('oauth_callback', $oAuthCallback);

        return $this;
    }

    public function setOAuthSignatureMethod($oAuthSignatureMethod)
    {
        $this->setArgument('oauth_signature_method', $oAuthSignatureMethod);

        return $this;
    }

    public function setOAuthVersion($oAuthVersion)
    {
        $this->setArgument('oauth_version', $oAuthVersion);

        return $this;
    }


}