<?php

/**
 * Class TwFlickrRestPhotosDeleteCall
 *
 * @method setApiKey(string $apiKey)
 * @method setPhotoId(string $photoId)
 */
class TwFlickrRestPhotosDeleteCall extends TwFlickrRestServiceCall
{
    public function __construct()
    {
        parent::__construct();

        $this->setArgument('method', 'flickr.photos.delete')
            ->allowArgument('api_key')
            ->allowArgument('photo_id');
    }
}