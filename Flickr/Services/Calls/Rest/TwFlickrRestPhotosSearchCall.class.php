<?php


/**
 * Class TwFlickrRestPhotosSearchCall
 *
 * @method  setApiKey(string $apyKey)
 * @method  setUserId(string $userId)
 * @method  setPage(integer $page)
 * @method  setMaxTakenDate(string $dateTime)
 * @method  setMinTakenDate(string $dateTime)
 * @method  setSort(string $criteria)
 * @method  setTagMode(string $tagMode)
 */
class TwFlickrRestPhotosSearchCall extends TwFlickrRestServiceCall
{
    const SORT_DATE_TAKEN_ASC       = 'date-taken-asc';
    const SORT_DATE_TAKEN_DESC      = 'date-taken-desc';
    const SORT_DATE_POSTED_ASC      = 'date-posted-asc';
    const SORT_DATE_POSTED_DESC     = 'date-posted-asc';
    const SORT_INTERESTINGNESS_ASC  = 'interestingness-asc';
    const SORT_INTERESTINGNESS_DESC = 'interestingness-desc';
    const SORT_RELEVANCE            = 'relevance';

    /**
     * Class constructor
     */
    public function __construct()
    {
        parent::__construct();

        $this->setArgument('method', 'flickr.photos.search')
            ->allowArgument('api_key')
            ->allowArgument('user_id')
            ->allowArgument('min_taken_date')
            ->allowArgument('max_taken_date')
            ->allowArgument('extras', 'date_taken, description')
            ->allowArgument('per_page')
            ->allowArgument('page')
            ->allowArgument('sort')
            ->allowArgument('tags')
            ->allowArgument('tag_mode');
    }

    public function setTags($tags = array())
    {
        $this->setArgument('tags', implode(',', $tags));

        return $this;
    }

    public function setExtras($extras = array())
    {
        $this->setArgument('extras', implode(',', $extras));

        return $this;
    }
}