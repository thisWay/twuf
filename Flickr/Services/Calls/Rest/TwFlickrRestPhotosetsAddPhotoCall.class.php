<?php

/**
 * Class TwFlickrRestPhotosetsCreateCall
 *
 * @method setApiKey(string $apiKey)
 * @method setPhotosetId(string $photosetId)
 * @method setPhotoId(string $photoId)
 */
class TwFlickrRestPhotosetsAddPhotoCall extends TwFlickrRestServiceCall
{
    public function __construct()
    {
        parent::__construct();

        $this->setArgument('method', 'flickr.photosets.addPhoto')
            ->allowArgument('api_key')
            ->allowArgument('photoset_id')
            ->allowArgument('photo_id');
    }
}