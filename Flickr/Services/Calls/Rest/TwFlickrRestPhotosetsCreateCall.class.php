<?php

/**
 * Class TwFlickrRestPhotosetsCreateCall
 *
 * @method setApiKey(string $apiKey)
 * @method setTitle(string $title)
 * @method setDescription(string $description)
 * @method setPrimaryPhotoId(string $primaryPhotoId)
 */
class TwFlickrRestPhotosetsCreateCall extends TwFlickrRestServiceCall
{
    public function __construct()
    {
        parent::__construct();

        $this->setArgument('method', 'flickr.photosets.create')
            ->allowArgument('api_key')
            ->allowArgument('title')
            ->allowArgument('description')
            ->allowArgument('primary_photo_id');
    }
}