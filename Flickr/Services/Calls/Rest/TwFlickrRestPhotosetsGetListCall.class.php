<?php

/**
 * Class TwFlickrRestPhotosetsGetListCall
 *
 * @method setApiKey(string $apiKey)
 * @method setUserId(string $userId)
 * @method setPage(string $page)
 * @method setPerPage(string $perPage)
 * @method setPrimaryPhotoExtras(string $primaryPhotoExtras)
 */
class TwFlickrRestPhotosetsGetListCall extends TwFlickrRestServiceCall
{
    public function __construct()
    {
        parent::__construct();

        $this->setArgument('method', 'flickr.photosets.getList')
            ->allowArgument('api_key')                  // required
            ->allowArgument('user_id')                  // optional
            ->allowArgument('page')                     // optional
            ->allowArgument('per_page')                 // optional
            ->allowArgument('primary_photo_extras');    // optional
    }
}