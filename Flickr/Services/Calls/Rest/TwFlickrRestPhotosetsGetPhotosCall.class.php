<?php

/**
 * Class TwFlickrRestPhotosetsGetPhotosCall
 *
 * @method setApiKey(string $apiKey)
 * @method setPhotosetId(string $photosetId)
 * @method setPage(string $page)
 * @method setPerPage(string $perPage)
 * @method setExtras(string $extras)
 */
class TwFlickrRestPhotosetsGetPhotosCall extends TwFlickrRestServiceCall
{
    public function __construct()
    {
        parent::__construct();

        $this->setArgument('method', 'flickr.photosets.getPhotos')
            ->allowArgument('api_key')                  // required
            ->allowArgument('photoset_id')              // required
            ->allowArgument('page')                     // optional
            ->allowArgument('per_page')                 // optional
            ->allowArgument('extras');                  // optional
    }
}