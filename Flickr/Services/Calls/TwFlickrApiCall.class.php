<?php

/**
 * Class TwFlickrApiCall
 *
 * @method setOauthNonce(string $oAuthNonce)
 * @method setOauthTimestamp(string $oAuthTimestamp)
 * @method setOauthToken(string $oAuthToken)
 * @method setOauthConsumerKey(string $oAuthConsumerKey)
 * @method setOauthSignature(string $oAuthSignature)
 */
class TwFlickrApiCall
{
    protected $endPoint;
    protected $httpMethod;
    protected $arguments    = array();

    public function __construct()
    {
        // offer support for authenticated requests
        $this->allowArgument('oauth_nonce')
        ->allowArgument('oauth_timestamp')
        ->allowArgument('oauth_signature_method', 'HMAC-SHA1')
        ->allowArgument('oauth_version', '1.0')
        ->allowArgument('oauth_token')
//        ->allowArgument('oauth_token_secret')
        ->allowArgument('oauth_signature')
        ->allowArgument('oauth_consumer_key');
    }

    public function setArgument($name, $value)
    {
        if (array_key_exists($name, $this->arguments))
        {
            $this->arguments[$name]	= $value;
        }
        else
        {
            throw new Exception(sprintf('Argument "%s" is not allowed on %s', $name, __CLASS__));
        }

        return $this;
    }

    public function getArgument($name)
    {
        return $this->arguments[$name];
    }

    protected function allowArgument($name, $default = null)
    {
        $this->arguments[$name] = $default;

        return $this;
    }

    public function getConfiguredArguments()
    {
        return $this->arguments;
    }

    public function getHttpMethod()
    {
        if (is_null($this->httpMethod))
        {
            throw new Exception(sprintf('Did you forget to set the "httpMethod" attribute for %s?', __CLASS__));
        }

        return $this->httpMethod;
    }

    protected function setHttpMethod($method)
    {
        $this->httpMethod   = $method;

        return $this;
    }

    public function getEndPoint()
    {
        if (is_null($this->endPoint))
        {
            throw new Exception(sprintf('Did you forget to set the "endPoint" attribute for %s?', __CLASS__));
        }

        return $this->endPoint;
    }

    protected function setEndPoint($endPoint)
    {
        $this->endPoint = $endPoint;

        return $this;
    }

    public function __call($method, $arguments)
    {
        if ('set' == substr($method, 0, 3))
        {
            $attribute  = TwStringsTool::camelCasedToSnakeCased(substr($method, 3));

            if (array_key_exists($attribute, $this->arguments))
            {
                return $this->setArgument($attribute, $arguments[0]);
            }
            elseif (property_exists($this, $attribute))
            {
                $this->{$attribute} = $arguments[0];

                return NULL;
            }

            throw new Exception('Method "' . $method .'" does not exist on this call class "' . get_called_class() . '".');
        }
    }

    public function getRequestUrl()
    {
        $url    = $this->getEndPoint() . '/' . $this->getArgument('method');

        if ('GET' == $this->getHttpMethod())
        {
            $url .= '?' . http_build_query($this->getConfiguredArguments());
        }

        return $url;
    }
}