<?php

class TwFlickrOAuthServiceCall extends TwFlickrApiCall
{
    protected $endPoint     = 'https://www.flickr.com/services/oauth';
    protected $httpMethod   = 'POST';

    public function __construct()
    {
        parent::__construct();

        $this->allowArgument('method');
    }


/*    protected $serviceMethod;

    protected function setServiceMethod($method)
    {
        $this->serviceMethod    = $method;

        return $this;
    }

    public function getServiceMethod()
    {
        if (is_null($this->serviceMethod))
        {
            throw new Exception('Did you forget to set the serviceMethod for this Flickr call?');
        }

        return $this->serviceMethod;
    }
*/
}