<?php

/**
 * Class TwFlickrRestServiceCall
 *
 * @method setFormat(string $format)
 * @method setNojsoncallback(string $noJsonCallBack)
 */
class TwFlickrRestServiceCall extends TwFlickrApiCall
{
    protected $endPoint     = 'https://www.flickr.com/services/rest';
    protected $httpMethod   = 'POST';

    public function __construct()
    {
        parent::__construct();

        $this->allowArgument('method')
            ->allowArgument('format', 'json')
            ->allowArgument('nojsoncallback', '1');
    }
}