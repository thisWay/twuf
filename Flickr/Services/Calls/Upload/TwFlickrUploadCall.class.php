<?php

class TwFlickrUploadCall extends TwFlickrApiCall
{
    protected $endPoint = 'http://www.flickr.com/services/upload/';

    public function __construct()
    {
        $this->allowArgument('photo')
            ->allowArgument('title')
            ->allowArgument('description')
            ->allowArgument('oauth_nonce')
            ->allowArgument('oauth_timestamp')
            ->allowArgument('oauth_signature_method', 'HMAC-SHA1')
            ->allowArgument('oauth_version', '1.0')
            ->allowArgument('oauth_token')
            ->allowArgument('oauth_token_secret')
            ->allowArgument('oauth_signature')
            ->allowArgument('oauth_consumer_key')
            ->setHttpMethod('POST');
    }

    public function getServiceMethod()
    {
        return '';
    }

    public function setPhoto($photo)
    {
        $this->setArgument('photo', $photo);

        return $this;
    }

    public function setTitle($title)
    {
        $this->setArgument('title', $title);

        return $this;
    }

    public function setDescription($description)
    {
        $this->setArgument('description', $description);

        return $this;
    }

    public function setOAuthNonce($oAuthNonce)
    {
        $this->setArgument('oauth_nonce', $oAuthNonce);

        return $this;
    }

    public function setOAuthTimeStamp($oAuthTimeStamp)
    {
        $this->setArgument('oauth_timestamp', $oAuthTimeStamp);

        return $this;
    }

    public function setOAuthConsumerKey($oAuthConsumerKey)
    {
        $this->setArgument('oauth_consumer_key', $oAuthConsumerKey);

        return $this;
    }

    public function setOAuthSignature($oAuthSignature)
    {
        $this->setArgument('oauth_signature', $oAuthSignature);

        return $this;
    }

    public function setOAuthSignatureMethod($oAuthSignatureMethod)
    {
        $this->setArgument('oauth_signature_method', $oAuthSignatureMethod);

        return $this;
    }

    public function setOAuthVersion($oAuthVersion)
    {
        $this->setArgument('oauth_version', $oAuthVersion);

        return $this;
    }

    public function setOAuthToken($oAuthToken)
    {
        $this->setArgument('oauth_token', $oAuthToken);

        return $this;
    }

    public function setOAuthTokenSecret($oAuthTokenSecret)
    {
        $this->setArgument('oauth_token_secret', $oAuthTokenSecret);

        return $this;
    }
}