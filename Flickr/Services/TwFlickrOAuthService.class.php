<?php

class TwFlickrOAuthService extends TwFlickrService
{
    /** @var  TwFlickrAccount */
    protected $account;
    /** @var TwFlickrAccountRepository */
    protected $accountRepository;


    /** @var TwMySqlPdo */
//    protected $tokenStorage;

    public function __construct($context = array())
    {
        parent::__construct($context['config'], $context['httpClient']);

        $this->setAccountRepository($context['accountRepository']);
    }

    protected function setAccountRepository(TwFlickrAccountRepository $repository)
    {
        $this->accountRepository    = $repository;

        return $this;
    }

    protected function getAccountRepository()
    {
        return $this->accountRepository;
    }


















    /**
     *
     */
    public function authorize()
    {
        // get the Flickr OAuth Token
        $requestToken   = $this->requestToken();

        // store the token in the database
        // TODO: make a wrapper for mysql tables
        $sql    = 'INSERT INTO `robobyte_thisway`.`tw_flickr_oauth_request_token` SET
            oauth_token = "' . $requestToken->getOAuthToken() . '",
            oauth_token_secret = "' . $requestToken->getOAuthTokenSecret() .'";';
        $this->getTokenStorage()->run($sql);

        $call   = new TwFlickrOAuthAuthorizeCall();
        $call->setOAuthToken($requestToken->getOAuthToken())
            ->setPerms($this->getConfig()->get('perms'));

        $authorizeUrl   = $this->getEndPoint() . $call->getServiceMethod() . '?' . http_build_query($call->getConfiguredArguments());

        header('Location: ' . $authorizeUrl);
        exit;
    }








    /*

        protected function setTokenStorage($tokenStorage)
        {
            $this->tokenStorage = $tokenStorage;

            return $this;
        }

        protected function getTokenStorage()
        {
            return $this->tokenStorage;
        }
    */

    /**
     * @return TwFlickrOAuthToken
     */
    public function requestToken()
    {
        $call   = new TwFlickrOAuthRequestTokenCall();

        $call->setOAuthCallback($this->getConfig()->get('oauth_callback'))
            ->setOAuthConsumerKey($this->getConfig()->get('oauth_consumer_key'))
            ->setOAuthNonce($this->getOAuthNonce())
            ->setOAuthTimeStamp($this->getOAuthTimeStamp())
            ->setOAuthSignature($this->getOAuthSignature($call));


//        $requestUrl = $this->getEndPoint() . $call->getServiceMethod();
        $requestUrl = $call->getRequestUrl();

        $requestUrl = $call->getEndPoint() . '/' . $call->getArgument('method');

        $forwardArguments   = $call->getConfiguredArguments();
        unset($forwardArguments['method']);
        unset($forwardArguments['oauth_token']);

        $response   = $this->httpRequest($requestUrl, $forwardArguments , $call->getHttpMethod());

        print_r($response . PHP_EOL);
        die(__METHOD__ . ' @ ' . __LINE__);
        $response   = $this->getResponseAsArray($response);

        if (isset($response['oauth_callback_confirmed']) && $response['oauth_callback_confirmed'])
        {
            $requestToken	= new TwFlickrOAuthToken($response['oauth_token'], $response['oauth_token_secret']);
        }

        return $requestToken;
    }

    /**
     * @return int
     */
    public function getOAuthNonce()
    {
        return rand(10000000, 99999999);
    }

    /**
     * @return int
     */
    public function getOAuthTimeStamp()
    {
        return TwTime::getTimeStamp();
    }

    /**
     * @param TwFlickrOAuthServiceCall $call
     * @return string
     */
    public function getOAuthSignature(TwFlickrOAuthServiceCall $call)
    {
        $arguments          = $call->getConfiguredArguments();
        $encodedArguments   = array();

        ksort($arguments);

        //TODO: exclude more elegantly support parameters
        foreach ($arguments as $name => $value)
        {
            if (!is_null($value) && $name != 'photo' && $name != 'method')
            {
                $encodedArguments[] = rawurlencode($name) . '=' . rawurlencode($value);
            }
        }

        $base   = rawurlencode($call->getHttpMethod()) . '&' . rawurlencode($call->getEndPoint() . $call->getServiceMethod()) . '&' . rawurlencode(implode('&', $encodedArguments));
        $key    = $this->getConfig()->get('oauth_consumer_secret') . '&' . (!is_null($this->getAccount(FALSE)) ? $this->getAccount()->getOAuthTokenSecret() : '');

        return base64_encode(hash_hmac('sha1', $base, $key, true));
    }


    public function accessToken($oAuthToken, $oAuthVerifier)
    {
        // get matching token from storage
        $token  = new TwFlickrOAuthToken();

        $sql    = 'SELECT * FROM `robobyte_thisway`.`tw_flickr_oauth_request_token` WHERE `oauth_token` = "' . $oAuthToken . '" AND used = 0;';
        $result = $this->getTokenStorage()->run($sql);

        $validToken = FALSE;
        foreach ($result as $row)
        {
            $validToken = TRUE;
            $token->copyAttributesFromObject(TwObject::fromArray($row));
        }

        if (!$validToken)
        {
            die('redirect spre request token step');
        }

        $token->setOAuthVerifier($oAuthVerifier);

        $call   = new TwFlickrOAuthAccessTokenCall();
        $call->setOAuthNonce($this->getOAuthNonce())
            ->setOAuthTimeStamp($this->getOAuthTimeStamp())
            ->setOAuthVerifier($oAuthVerifier)
            ->setOAuthToken($oAuthToken)
            ->setOAuthTokenSecret($token->getOAuthTokenSecret())
            ->setOAuthConsumerKey($this->getConfig()->get('oauth_consumer_key'))
            ->setOAuthSignature($this->getOAuthSignature($call));

        $requestUrl = $this->getEndPoint() . $call->getServiceMethod();

        $response   = $this->httpRequest($requestUrl, $call->getConfiguredArguments(), $call->getHttpMethod());

        $response   = $this->getResponseAsArray($response);

       //$token->setOAuthVerifier();


        // save the complete token
        // TODO: make a wrapper for mysql tables
        $sql    = 'UPDATE `robobyte_thisway`.`tw_flickr_oauth_request_token` SET
            `used` = 1 WHERE oauth_token = "' . $token->getOAuthToken() . '";';
        $this->getTokenStorage()->run($sql);


        if (isset($response['username']))
        {
            // save the flickr account
            $sql    = 'INSERT INTO `robobyte_thisway`.`tw_flickr_account` SET
            `username` = "' . urldecode($response['username']) . '",
            `fullname` = "' . urldecode($response['fullname']) . '",
            `user_nsid` = "' . urldecode($response['user_nsid']) . '",
            `oauth_token` = "' . urldecode($response['oauth_token']) . '",
            `oauth_token_secret` = "' . urldecode($response['oauth_token_secret']) . '";';

            $this->getTokenStorage()->run($sql);

            $flickrAccount  = new TwFlickrAccount();
            $flickrAccount->copyAttributesFromObject(TwObject::fromArray($response));
        }

        return $flickrAccount;
    }

    public function getApiKey()
    {
        return $this->getConfig()->get('oauth_consumer_key');
    }

    public function getApiSecret()
    {
        return $this->getConfig()->get('oauth_consumer_secret');
    }


    /**
     * Search the Flickr account in the database, if none found authorize application
     *
     * @param bool $tryLogin
     * @return TwFlickrAccount|void
     */
    public function getAccount($tryLogin = TRUE)
    {
        if (!is_null($this->account) || !$tryLogin)
        {
            return $this->account;
        }

        $account    = $this->getAccountRepository()->findOneByUsername($this->getConfig()->get('account_username'));

        $this->account  = $account ? $account : $this->authorize();

        return $this->account;
    }
}