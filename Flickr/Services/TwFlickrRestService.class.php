<?php

class TwFlickrRestService extends TwFlickrService
{
    public function photosSearch(TwFlickrRestPhotosSearchCall $call)
    {
        $response   = $this->getHttpClient()->requestJson($call->getRequestUrl(), $call->getConfiguredArguments());

        if (isset($response->stat) && 'ok' == $response->stat)
        {
            $photos = array();
            if ($response->photos->total > 0)
            {
                foreach ($response->photos->photo as $photo)
                {
                    $flickrPhoto    = new TwFlickrPhoto();
                    $flickrPhoto->copyAttributesFromObject($photo);
                    $photos[]       = $flickrPhoto;
                }
            }

            return $photos;
        }

        return FALSE;
    }

    /**
     * @param TwFlickrRestPhotosetsGetListCall $call
     * @return string
     * @throws Exception
     */
    public function photosetsGetList(TwFlickrRestPhotosetsGetListCall $call)
    {
        $response   = $this->getHttpClient()->requestJson($call->getRequestUrl(), $call->getConfiguredArguments());

        if (isset($response->stat) && 'ok' == $response->stat)
        {
            $photosets  = array();
            if ($response->photosets->total > 0)
            {
                foreach ($response->photosets->photoset as $photoset)
                {
                    $photoAttributes = array(
                        'id'        => $photoset->primary,
                        'farm'      => $photoset->farm,
                        'server'    => $photoset->server,
                        'secret'    => $photoset->secret,
                        'takenAt'   => $photoset->primary_photo_extras->datetaken,
                    );

                    $primaryPhoto   = new TwFlickrPhoto();
                    $primaryPhoto->setIsPrimary(TRUE);
                    $primaryPhoto->copyAttributesFromArray($photoAttributes);

                    $flickrPhotoset = new TwFlickrPhotoset($primaryPhoto);
                    $flickrPhotoset->copyAttributesFromObject($photoset);
                    $photosets[]    = $flickrPhotoset;
                }
            }

            return $photosets;
        }
        elseif (isset($response->stat))
        {
            throw new Exception($response->message, $response->code);
        }

        return FALSE;
    }


    /**
     * @param TwFlickrRestPhotosetsGetPhotosCall $call
     * @return string
     * @throws Exception
     */
    public function photosetsGetPhotos(TwFlickrRestPhotosetsGetPhotosCall $call)
    {
        $response   = $this->getHttpClient()->requestJson($call->getRequestUrl(), $call->getConfiguredArguments());

//        print_r($call);
//        print_r($response);

        if (isset($response->stat) && 'ok' == $response->stat)
        {
            $photos = array();
            if ($response->photoset->total > 0)
            {
                foreach ($response->photoset->photo as $photo)
                {
                    $flickrPhoto    = new TwFlickrPhoto();
                    $flickrPhoto->copyAttributesFromObject($photo);

                    $photos[]   = $flickrPhoto;
                }
            }

            return $photos;
        }
        elseif (isset($response->stat))
        {
            throw new Exception($response->message, $response->code);
        }

        return FALSE;
    }


    /**
     * @param TwFlickrRestPhotosetsCreateCall $call
     * @return string
     * @throws Exception
     */
    public function photosetsCreate(TwFlickrRestPhotosetsCreateCall $call)
    {
        $response   = $this->getHttpClient()->requestJson($call->getRequestUrl(), $call->getConfiguredArguments());

        if (isset($response->stat) && 'ok' != $response->stat)
        {
            throw new Exception($response->message, $response->code);
        }

        return $response->photoset->id;
    }

    /**
     * @param TwFlickrRestPhotosetsAddPhotoCall $call
     * @return bool
     * @throws Exception
     */
    public function photosetsAddPhoto(TwFlickrRestPhotosetsAddPhotoCall $call)
    {
        $response   = $this->getHttpClient()->requestJson($call->getRequestUrl(), $call->getConfiguredArguments());

        if (isset($response->stat) && 'ok' != $response->stat)
        {
            throw new Exception($response->message, $response->code);
        }

        return TRUE;
    }





    public function photosDelete(TwFlickrRestPhotosDeleteCall $call)
    {
        $response   = $this->getHttpClient()->requestJson($call->getRequestUrl(), $call->getConfiguredArguments());

        if (isset($response->stat) && 'ok' != $response->stat)
        {
            throw new Exception($response->message, $response->code);
        }

        return TRUE;    }
}