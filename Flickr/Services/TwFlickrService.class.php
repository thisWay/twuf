<?php

class TwFlickrService
{
    protected $endPoint;
    /** @var  TwConfig */
    protected $config;
    /** @var TwHttpClient */
    protected $httpClient;

    public function __construct(TwConfig $config, TwHttpClient $httpClient)
    {
        $this->setConfig($config)
            ->setHttpClient($httpClient);
    }

    /**
     * @param TwConfig $config
     * @return $this
     */
    protected function setConfig(TwConfig $config)
    {
        $this->config   = $config;

        return $this;
    }

    /**
     * @return TwConfig
     */
    protected function getConfig()
    {
        return $this->config;
    }

    /**
     * @param TwHttpClient $httpClient
     * @return $this
     */
    protected function setHttpClient(TwHttpClient $httpClient)
    {
        $this->httpClient   = $httpClient;

        return $this;
    }

    /**
     * @return TwHttpClient
     */
    protected function getHttpClient()
    {
        return $this->httpClient;
    }

    protected function httpRequest($url, $arguments, $method)
    {
        print_r(func_get_args());

        if ('POST' == $method)
        {
            return $this->getHttpClient()->post($url, $arguments);
        }

        if ('GET' == $method)
        {
            return $this->getHttpClient()->get($url, $arguments);
        }
    }

    /**
     * Converts the weird Flickr response string into an array
     *
     * @returns array
     */
    public function getResponseAsArray($response)
    {
        $result             = array();
        $pairs              = explode('&', $response);
        $lastValidPairKey   = 0;
        $lastValidPair      = 0;

        for ($i = 0; $i < count($pairs); $i++)
        {
            $parts  = explode('=', $pairs[$i]);

            if (count($parts) == 2)
            {
                $result[$parts[0]]  = $parts[1];
                $lastValidPairKey   = $parts[0];
                $lastValidPair      = $i;
            }
            elseif (count($parts == 1))
            {
                $result[$lastValidPairKey]  .= $parts[0];
            }
        }

        return $result;
    }
}