<?php

class TwFlickrUploadService extends TwFlickrService
{
    /**
     * @param TwFlickrUploadCall $call
     * @return TwFlickrPhoto
     */
    public function upload(TwFlickrUploadCall $call)
    {
        $arguments  = $call->getConfiguredArguments();

        //TODO: exclude more elegantly support parameters
        foreach ($arguments as $name => $value)
        {
            if (is_null($value))
            {
                unset($arguments[$name]);
            }
        }

        $response   = $this->getHttpClient()->post($call->getEndPoint(), $arguments);

        try
        {
            $response   = new SimpleXMLElement($response);

                if ('ok' == (string)$response->attributes()->stat)
                {
                    $flickrPhoto    = new TwFlickrPhoto();
                    $flickrPhoto->setId((string)$response->photoid)
                        ->setTitle($call->getArgument('title'))
                        ->setDescription($call->getArgument('description'));

                    return $flickrPhoto;

                }
        }
        catch (Exception $e)
        {}

        return FALSE;
    }
}