<?php

class TwFlickrAccount extends TwObject
{
    protected $id;
    protected $userNsid;
    protected $userName;
    protected $fullName;
    protected $oAuthToken;
    protected $oAuthTokenSecret;

    static $mapping  = array(
        'id'                    => 'id',
        'user_nsid'             => 'userNsid',
        'username'              => 'userName',
        'fullname'              => 'fullName',
        'oauth_token'           => 'oAuthToken',
        'oauth_token_secret'    => 'oAuthTokenSecret',
    );

    public function getUserNsid()
    {
        return $this->userNsid;
    }

    public function getOAuthToken()
    {
        return $this->oAuthToken;
    }

    public function getOAuthTokenSecret()
    {
        return $this->oAuthTokenSecret;
    }
}