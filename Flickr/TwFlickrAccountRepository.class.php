<?php

class TwFlickrAccountRepository extends TwMySqlRepository
{
    protected $tableName    = 'tw_flickr_account';
    protected $primaryKey   = 'id';

    public function findOneByUsername($userName)
    {
        $sql    = 'SELECT * FROM `' . $this->getDatabase() . '`.`' . $this->getTable() . '` WHERE `username` = "' . $userName . '";';
        $result = $this->getMySqlPdo()->run($sql);

        foreach ($result as $row)
        {
            $account    = new TwFlickrAccount();

            $account->copyAttributesFromObject(TwObject::arrayToObject($row));

            return $account;
        }

        return FALSE;
    }
}