<?php

class TwFlickrApplication
{
    /** @var  TwConfig */
    protected $config;
    /** @var  TwFlickrOAuthService */
    protected $oAuthService;
    /** @var  TwFlickrRestService */
    protected $restService;
    /** @var  TwFlickrUploadService */
    protected $uploadService;
    /** @var TwHttpClient */
    protected $httpClient;



    /** @var TwMySqlPdo */
    protected $tokenStorage;

    public function __construct(TwConfig $config)
    {
        $this->setConfig($config)
            ->setHttpClient(new TwHttpClient());
    }

    protected function setConfig(TwConfig $config)
    {
        $this->config   = $config;

        return $this;
    }

    protected function getConfig()
    {
        return $this->config;
    }

    protected function setHttpClient(TwHttpClient $httpClient)
    {
        $this->httpClient   = $httpClient;

        return $this;
    }

    protected function getHttpClient()
    {
        return $this->httpClient;
    }

    public function getOAuthService()
    {
        if (is_null($this->oAuthService))
        {
            $this->initOAuthService();
        }

        return $this->oAuthService;
    }

    protected function initOAuthService()
    {
        $context    = array(
            'config'            => $this->getConfig()->getSubConfig('service.oauth'),
            'httpClient'        => $this->getHttpClient(),
            'accountRepository' => new TwFlickrAccountRepository(new TwMySqlPdo($this->getConfig()->getSubConfig('account_repository'))),
        );

        $this->oAuthService     = new TwFlickrOAuthService($context);

        return $this->oAuthService;
    }

    public function getRestService()
    {
        if (is_null($this->restService))
        {
            return $this->initRestService();
        }

        return $this->restService;
    }

    protected function initRestService()
    {
        $this->restService  = new TwFlickrRestService($this->getConfig()->getSubConfig('service.rest'), $this->getHttpClient());

        return $this->restService;
    }

    protected function getUploadService()
    {
        if (is_null($this->uploadService))
        {
            return $this->initUploadService();
        }

        return $this->uploadService;
    }

    protected function initUploadService()
    {
        $this->uploadService    = new TwFlickrUploadService($this->getConfig()->getSubConfig('service.upload'), $this->getHttpClient());

        return $this->uploadService;
    }

    protected function getOAuthSignature($call)
    {
        return $this->getOAuthService()->getOAuthSignature($call);
    }

    protected function signCall(TwFlickrApiCall $call)
    {
        $call->setOauthNonce($this->getOAuthService()->getOAuthNonce());
        $call->setOauthTimestamp($this->getOAuthService()->getOAuthTimeStamp());
        $call->setOauthToken($this->getOAuthService()->getAccount()->getOAuthToken());
        $call->setOauthConsumerKey($this->getOAuthService()->getApiKey());
        $call->setOauthSignature($this->getOAuthService()->getOAuthSignature($call));

        return $call;
    }






    /** Public functions / Flickr API interactions **/

    public function uploadImage(TwFlickrPhoto $flickrPhoto, TwFlickrAccount $account = null)
    {
/*        if (is_null($account))
        {
            $account    = $this->getAccount($this->getConfig()->get('account_username'));
        }
*/
        $call   = new TwFlickrUploadCall();
        $call->setPhoto('@' . $flickrPhoto->getTemporaryFile())
            ->setTitle($flickrPhoto->getTitle())
            ->setDescription($flickrPhoto->getDescription())
            ->setOAuthToken($account->getOAuthToken())
            ->setOAuthTokenSecret($account->getOAuthTokenSecret())
            ->setOAuthNonce($this->getOAuthService()->getOAuthNonce())
            ->setOAuthTimeStamp($this->getOAuthService()->getOAuthTimeStamp())
            ->setOAuthConsumerKey($this->getOAuthService()->getApiKey())
            ->setOAuthSignature($this->getOAuthSignature($call));

        return  $this->getUploadService()->upload($call);
    }




    public function photosetsCreate(TwFlickrPhotoset $photoset, TwFlickrAccount $account = null)
    {
/*        if (is_null($account))
        {
            $account    = $this->getAccount($this->getConfig()->get('account_username'));
        }
*/
        $call   = new TwFlickrRestPhotosetsCreateCall();

        $call->setApiKey($this->getOAuthService()->getApiKey());
        $call->setTitle($photoset->getTitle());
        $call->setDescription($photoset->getTitle());
        $call->setPrimaryPhotoId($photoset->getPrimaryPhoto()->getId());

        $call   = $this->signCall($call);

        $newPhotosetId  = $this->getRestService()->photosetsCreate($call);

        $photoset->setId($newPhotosetId);

        return $photoset;
    }





    // September 2014
    public function getApiKey()
    {
        return $this->getOAuthService()->getApiKey();
    }


    public function deletePhotoById($photoId)
    {
        $call   = new TwFlickrRestPhotosDeleteCall();

        $call->setPhotoId($photoId);
        $call->setApiKey($this->getApiKey());

        $call   = $this->signCall($call);

        return $this->getRestService()->photosDelete($call);
    }



    /**
     * Delete one photo from Flicker
     *
     * @param $photoId
     *
    public function photosDelete($photoId)
    {
        $call   = new TwFlickrRestPhotosDeleteCall();

        $call->setPhotoId($photoId);
        $call->setApiKey($this->getOAuthService()->getApiKey());

        $call   = $this->signCall($call);

        return $this->getRestService()->photosDelete($call);
    }


*/

    /**
     * @param TwFlickrAccount $account
     * @return TwFlickrPhotoset[]
     */
    public function photosetsGetList(TwFlickrAccount $account = null)
    {
        $call   = new TwFlickrRestPhotosetsGetListCall();
        $call->setPrimaryPhotoExtras('date_taken');

        $call->setApiKey($this->getOAuthService()->getApiKey());

        $call   = $this->signCall($call);

        return $this->getRestService()->photosetsGetList($call);
    }

    public function photosetsGetPhotos(TwFlickrPhotoset $photoset)
    {
        $photos = array();
        $duplicates = array();

        $call   = new TwFlickrRestPhotosetsGetPhotosCall();
        $call->setApiKey($this->getOAuthService()->getApiKey());
        $call->setPhotosetId($photoset->getId());
        $call->setExtras('date_taken');
        $call->setPerPage(500);

        for ($i = 1; $i <= ceil($photoset->getPhotosNo() / 500); $i++)
        {
            $call->setPage($i);
            $call   = $this->signCall($call);

            $batch  = $this->getRestService()->photosetsGetPhotos($call);

            if (count($batch))
            {
                /* @var $photo TwFlickrPhoto */
                foreach($batch as $photo)
                {
                    if (isset($photos[$photo->getTakenAt()]))
                    {
                        $duplicates[$photo->getTakenAt()]  = array(
                            $photos[$photo->getTakenAt()],
                            $photo
                        );

                        $photos[$photo->getTakenAt() . ' ... ' . (count($photos[$photo->getTakenAt()]) + 1)]   = $photo;
                    }
                    else
                    {
                        $photos[$photo->getTakenAt()]   = $photo;
                    }
                }
            }

//            $photos = array_merge($photos, $batch);
        }

//        print_r($duplicates);

        ksort($photos);

//        print_r($photos);

//        die('all the photos sorted');
        return $photos;
    }

    public function loadPhotosIntoPhotoset(TwFlickrPhotoset $photoset)
    {
        $photos = $this->photosetsGetPhotos($photoset);

        if (count($photos))
        {
            /* @var $photo TwFlickrPhoto */
            foreach ($photos as $photo)
            {
                if ($photoset->getPrimaryPhoto() && $photo->getIsPrimary())
                {
                    continue;
                }

                $photoset->addPhoto($photo);
            }
        }

        return $photoset;
    }



























    public function getTokenStorage()
    {
        if (is_null($this->tokenStorage))
        {
            $this->tokenStorage = new TwMySqlPdo($this->getConfig()->getSubConfig('tokenStorage'));
        }

        return $this->tokenStorage;
    }




}