<?php

class TwFlickrOAuthToken extends TwObject
{
    protected $oAuthToken;
    protected $oAuthTokenSecret;
    protected $oAuthVerifier;

    protected $mapping  = array(
        'oauth_token'           => 'oAuthToken',
        'oauth_token_secret'    => 'oAuthTokenSecret',
        'oauth_verifier'        => 'oAuthVerifier',
    );

    public function __construct($oAuthToken = null, $oAuthTokenSecret = null)
    {
        $this->oAuthToken       = $oAuthToken;
        $this->oAuthTokenSecret = $oAuthTokenSecret;
    }

    public function getOAuthToken()
    {
        return $this->oAuthToken;
    }

    public function getOAuthTokenSecret()
    {
        return $this->oAuthTokenSecret;
    }

    public function setOAuthVerifier($oAuthVerifier)
    {
        $this->oAuthVerifier    = $oAuthVerifier;
    }

    public function getOAuthVerifier()
    {
        return $this->oAuthVerifier;
    }
}