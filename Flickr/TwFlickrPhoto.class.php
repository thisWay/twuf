<?php


class TwFlickrPhoto extends TwObject
{
    const SIZE_SQUARE           = '_s';     // 75 x 75
    const SIZE_LARGE_SQUARE     = '_q';     // 150 x 150
    const SIZE_THUMBNAIL        = '_t';     // 100 x H
    const SIZE_SMALL_240        = '_m';     // 240 x H
    const SIZE_SMALL320         = '_n';     // 320 x H
    const SIZE_MEDIUM_500       = '';       // 500 x H
    const SIZE_MEDIUM_640       = '_z';     // 640 x H
    const SIZE_MEDIUM_800       = '_c';     // 800 x H
    const SIZE_LARGE_1024       = '_b';     // 1024 x H
    const SIZE_LARGE_1600       = '_h';     // 1600 x H
    const SIZE_LARGE_2048       = '_k';     // 2048 x H
    const SIZE_ORIGINAL         = '_o';     // W x H
    const DOWNLOAD_SUFFIX       = '_d';

	protected $id;
	protected $secret;
	protected $originalSecret;
	protected $farm;
	protected $server;
	protected $title;
	protected $description;
	protected $isPrimary;
	protected $takenAt;

    protected $temporaryFile;

    static $mapping  = array(
        // input => target
        'id'            => 'id',
        'secret'        => 'secret',
        'farm'          => 'farm',
        'server'        => 'server',
        'title'         => 'title',
        'description'   => array(
            'attribute' => 'description',
            'callback'  => 'setDescription'
        ),
        'isprimary'     => 'isPrimary',
        'datetaken'     => 'takenAt',
        'takenAt'       => 'takenAt',
    );

    /**  Setters and getters **/

    public function setId($id)
    {
        $this->id   = $id;

        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setTitle($title)
    {
        $this->title    = $title;

        return $this;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setDescription($description)
    {
        // fix the Flickr weird behavior
        $this->description  = $description instanceof stdClass ? $description->_content : $description;

        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setIsPrimary($isPrimary)
    {
        $this->isPrimary    = $isPrimary;

        return $this;
    }

    public function getIsPrimary()
    {
        return $this->isPrimary;
    }




    public function setTemporaryFile($file)
    {
        if (!file_exists($file))
        {
            throw new FlickrFileNotFoundException("FlickrPhoto temporary file '{$file}' was not found.");
        }

        $this->temporaryFile    = $file;

        return $this;
    }

    public function getTemporaryFile()
    {
        return $this->temporaryFile;
    }


	private function validateAttributeIsSet($property)
	{
		if (!isset($this->{$property}))
		{
			throw new FlickrInvalidPhotoException("Invalid FlickrPhoto, missing value for '{$property}'.");
		}

		return $this;
	}

	private function validate()
	{
		$this->validateAttributeIsSet('id')
			->validateAttributeIsSet('secret')
			->validateAttributeIsSet('farm')
			->validateAttributeIsSet('server');

		return $this;
	}

    public function getDownloadUrl($size = self::SIZE_MEDIUM_640)
    {
        return str_replace('.jpg', self::DOWNLOAD_SUFFIX . '.jpg', $this->getUrl($size));
    }

    public function getUrl($size = self::SIZE_LARGE_1600)
    {
        $this->validate();

        if (self::SIZE_ORIGINAL === $size)
        {
            $this->validateAttributeIsSet('originalSecret');
        }

        $secret	= (self::SIZE_ORIGINAL === $size ? $this->originalSecret : $this->secret);

        return "https://farm{$this->farm}.staticflickr.com/{$this->server}/{$this->id}_{$secret}{$size}.jpg";
    }

    public function getTakenAt()
    {
        return $this->takenAt;
    }

    public function adjustTakenAt($offset)
    {
        $this->takenAt  = TwTime::addToDateTime($offset, $this->getTakenAt());
    }




}