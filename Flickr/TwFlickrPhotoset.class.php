<?php

class TwFlickrPhotoset extends TwTimeStampableObject
{
    protected $id;
    protected $title;
    protected $description;
    protected $createdAt;
    protected $updatedAt;
    protected $photosNo;

    /** @var TwFlickrPhoto[] */
    protected $photos;
    protected $primaryPhotoId;
    protected $duplicatePhotoIds    = array();
    protected $photoTakenAts        = array();

    static $mapping  = array(
        // input => target
        'id'            => 'id',
        'title'         => array(
            'callback'  => 'setTitle'
        ),
        'description'   => array(
            'attribute' => 'description',
            'callback'  => 'setDescription'
        ),
        'date_create'   => array(
            'callback'  => 'setCreatedAt',
        ),
        'date_update'   => array(
            'callback'  => 'setUpdatedAt',
        ),
        'photos'        => 'photosNo',
    );



    public function __construct(TwFlickrPhoto $photo)
    {
        parent::__construct();

        $photo->setIsPrimary(TRUE);
        $this->addPhoto($photo);
        $this->primaryPhotoId   = $photo->getId();
    }

    public function setId($id)
    {
        $this->id   = $id;

        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setTitle($title)
    {
        // fix the Flickr weird behavior
        $this->title    = $title instanceof stdClass ? $title->_content : $title;

        return $this;
    }

    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set the photoset's description
     *
     * @param $description
     * @return $this
     */
    public function setDescription($description)
    {
        // fix the Flickr weird behavior
        $this->description  = $description instanceof stdClass ? $description->_content : $description;

        return $this;
    }

    /**
     * Get the photoset's description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }



    /**
     * Returns the number of photos loaded in the photoset's photos list, not the total number of photos given by Flickr.com
     */
    public function getLoadedPhotosCount()
    {
        return count($this->photos);
    }

    public function getPhotosNo()
    {
        return $this->photosNo;
    }


    /**
     * Add a photo to the photoset
     *
     * @param TwFlickrPhoto $photo
     * @return $this
     */
    public function addPhoto(TwFlickrPhoto $photo)
    {
        // Add the photo to the list of photos
        $this->photos[$photo->getId()]  = $photo;

        // Check if there are other photos taken at the same time (most likely uploaded more than once)
        $duplicate  = isset($this->photoTakenAts[$photo->getTakenAt()]);

        $this->photoTakenAts[$photo->getTakenAt()][]    = $photo->getId();

        if ($duplicate)
        {
            $this->duplicatePhotoIds[$photo->getTakenAt()]  = $this->photoTakenAts[$photo->getTakenAt()];
        }

        return $this->onModify();
    }

    /**
     * Get the photoset's primary photo
     *
     * @return TwFlickrPhoto
     */
    public function getPrimaryPhoto()
    {
        return $this->photos[$this->primaryPhotoId];
    }


    /**
     * Get the photoset's list of photos
     *
     * @return TwFlickrPhoto[]
     */
    public function getPhotos()
    {
        return $this->photos;
    }

    /**
     * Get a list of duplicate photos sorted by the moment the photo was taken
     *
     * @return array
     */
    public function getDuplicatePhotos()
    {
        $photos = array();

        foreach ($this->duplicatePhotoIds as $photoTakenAt => $photoIds)
        {
            foreach ($photoIds as $photoId)
            {
                $photos[$photoId]   = $this->photos[$photoId];
            }
        }

        return $photos;
    }




    public function setCreatedAt($createdAt)
    {
        $this->createdAt    = date('Y-m-d H:i:s', $createdAt);

        return $this;
    }

    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt    = date('Y-m-d H:i:s', $updatedAt);

        return $this;
    }
}