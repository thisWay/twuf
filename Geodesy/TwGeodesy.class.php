<?php

class TwGeodesy
{
    static function getBoundsCentroid($coordinates = array())
    {
        $coordinatesCount   = count($coordinates);

        if (!$coordinatesCount)
        {
            return FALSE;
        }

        $bounds = self::getBounds($coordinates);

        $centerLatitude     = ($bounds->getSouthWest()->getLatitude() + $bounds->getNorthEast()->getLatitude()) /2;
        $centerLongitude    = ($bounds->getSouthWest()->getLongitude() + $bounds->getNorthEast()->getLongitude()) /2;

        $center = new TwGeodesyDdCoordinate($centerLatitude, $centerLongitude);

        $distances  = array();
        for ($i = 0; $i < $coordinatesCount ; $i++)
        {
            $distances[$i]  = self::getDistance($coordinates[$i], $center);
        }

        asort($distances);

        $distancesOrder = array_keys($distances);

        return $coordinates[$distancesOrder[0]];
    }

    static function getDistance(TwGeodesyDdCoordinate $coordinate_1, TwGeodesyDdCoordinate $coordinate_2)
    {
        return sqrt(
                pow(
                    abs($coordinate_1->getLatitude() - $coordinate_2->getLatitude()),
                    2
                ) + pow(
                    abs($coordinate_1->getLongitude() - $coordinate_2->getLongitude()),
                    2
                )
            );
    }

    static function getHaversineDistance(TwGeodesyDdCoordinate $point_1, TwGeodesyDdCoordinate $point_2)
    {
        return rad2deg(acos((sin(deg2rad($point_1->getLatitude()))*sin(deg2rad($point_2->getLatitude()))) + (cos(deg2rad($point_1->getLatitude()))*cos(deg2rad($point_2->getLatitude()))*cos(deg2rad($point_1->getLongitude()-$point_2->getLongitude())))));
    }

    static function getHaversineDistanceInKm(TwGeodesyDdCoordinate $point_1, TwGeodesyDdCoordinate $point_2)
    {
        return self::getHaversineDistance($point_1, $point_2) * 111.13384;
    }

    static function getBounds($coordinates = array())
    {
        $coordinatesCount   = count($coordinates);

        if (!$coordinatesCount)
        {
            return FALSE;
        }

        $bounds = new TwGeodesyBounds($coordinates[0]);

        for ($i = 1; $i < $coordinatesCount; $i++)
        {
            $bounds->extend($coordinates[$i]);
        }

        return $bounds;
    }

    static function convertDdToDms(TwGeodesyDdCoordinate $dd)
    {
        $dms            = new TwGeodesyDmsCoordinate();

        list($degrees, $minutes, $seconds)  = self::splitDDtoDMS($dd->getLatitude());

        $dms->setLatitude(new TwGeodesyDmsCoordinateLatitude($degrees, $minutes, $seconds));

        list($degrees, $minutes, $seconds)  = self::splitDDtoDMS($dd->getLongitude());

        $dms->setLongitude(new TwGeodesyDmsCoordinateLongitude($degrees, $minutes, $seconds));

        return $dms;
    }

    static function convertDmsToDd(TwGeodesyDmsCoordinate $dms)
    {
        $latitude   = self::joinDmsToDd($dms->getLatitude());
        $longitude  = self::joinDmsToDd($dms->getLongitude());

        return new TwGeodesyDdCoordinate($latitude, $longitude);
    }

    static function splitDdToDms($dd)
    {
        $degrees        = floor($dd);
        $minutesSeconds = bcsub($dd, $degrees, 6) * 60;
        $minutes        = floor($minutesSeconds);
        $seconds        = bcsub($minutesSeconds, $minutes, 6) * 60;

        return array($degrees, $minutes, $seconds);
    }

    static function joinDmsToDd(TwGeodesyDmsCoordinateDimension $dms)
    {
        return $dms->getDegrees() + $dms->getMinutes() / 60 + $dms->getSeconds() / 3600;
    }
}