<?php

class TwGeodesyBounds
{
    public $southWest;
    public $northEst;

    public function __construct(TwGeodesyDdCoordinate $southWest, TwGeodesyDdCoordinate $northEst = null)
    {
        if (is_null($northEst))
        {
            $northEst   = $southWest;
        }

        $this->southWest    = clone $southWest;
        $this->northEst     = clone $northEst;
    }

    public function getSouthWest()
    {
        return $this->southWest;
    }

    public function getNorthEast()
    {
        return $this->northEst;
    }

    public function extend(TwGeodesyDdCoordinate $coordinate)
    {
        $this->getSouthWest()->setLatitude(min($this->getSouthWest()->getLatitude(), $coordinate->getLatitude()));
        $this->getSouthWest()->setLongitude(min($this->getSouthWest()->getLongitude(), $coordinate->getLongitude()));

        $this->getNorthEast()->setLatitude(max($this->getNorthEast()->getLatitude(), $coordinate->getLatitude()));
        $this->getNorthEast()->setLongitude(max($this->getNorthEast()->getLongitude(), $coordinate->getLongitude()));
    }
}