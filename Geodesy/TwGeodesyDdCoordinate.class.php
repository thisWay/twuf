<?php

class TwGeodesyDdCoordinate
{
    public $latitude;
    public $longitude;

    public function __construct($latitude, $longitude)
    {
        $this->setLatitude($latitude);
        $this->setLongitude($longitude);
    }

    public function setLatitude($latitude)
    {
        if (strpos($latitude, 'N') || strpos($latitude, 'S'))
        {
            $this->latitude = (float)substr($latitude, 0, strlen($latitude) - 1);

            if (strpos($latitude, 'S'))
            {
                $this->latitude *= -1;
            }
        }
        else
        {
            $this->latitude = (float)$latitude;
        }
    }

    public function setLongitude($longitude)
    {
        if (strpos($longitude, 'E') || strpos($longitude, 'W'))
        {
            $this->longitude    = (float)substr($longitude, 0, strlen($longitude) - 1);

            if (strpos($longitude, 'W'))
            {
                $this->longitude    *= -1;
            }
        }
        else
        {
            $this->longitude    = (float)$longitude;
        }
    }

    public function getLatitude()
    {
        return $this->latitude;
    }

    public function getLongitude()
    {
        return $this->longitude;
    }
}