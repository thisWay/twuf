<?php

class TwGeodesyDmsCoordinate
{
    protected $latitude;
    protected $longitude;

    public function setLatitude(TwGeodesyDmsCoordinateLatitude $latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLatitude()
    {
        return $this->latitude;
    }

    public function setLongitude(TwGeodesyDmsCoordinateLongitude $longitude)
    {
        $this->longitude    = $longitude;

        return $this;
    }

    public function getLongitude()
    {
        return $this->longitude;
    }
}