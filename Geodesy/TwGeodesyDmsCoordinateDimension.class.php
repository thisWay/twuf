<?php

class TwGeodesyDmsCoordinateDimension
{
    protected $degrees;
    protected $minutes;
    protected $seconds;

    public function __construct($degrees, $minutes, $seconds)
    {
        $this->setDegrees($degrees);
        $this->setMinutes($minutes);
        $this->setSeconds($seconds);
    }

    public function setDegrees($degrees)
    {
        $this->degrees  = $degrees;

        return $this;
    }

    public function getDegrees()
    {
        return $this->degrees;
    }

    public function setMinutes($minutes)
    {
        $this->minutes  = $minutes;

        return $this;
    }

    public function getMinutes()
    {
        return $this->minutes;
    }

    public function setSeconds($seconds)
    {
        $this->seconds  = $seconds;

        return $this;
    }

    public function getSeconds()
    {
        return $this->seconds;
    }
}