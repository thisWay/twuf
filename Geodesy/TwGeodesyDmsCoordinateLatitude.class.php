<?php

class TwGeodesyDmsCoordinateLatitude extends TwGeodesyDmsCoordinateDimension
{
    public function getHemisphere()
    {
        return $this->degrees > 0 ? 'N' : 'S';
    }
}