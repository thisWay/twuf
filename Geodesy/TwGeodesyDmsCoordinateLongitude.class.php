<?php

class TwGeodesyDmsCoordinateLongitude extends TwGeodesyDmsCoordinateDimension
{
    public function getHemisphere()
    {
        return $this->degrees > 0 ? 'E' : 'W';
    }
}