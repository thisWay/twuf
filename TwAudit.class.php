<?php

class TwAudit
{
    const INFO      = 'INFO';
    const NOTICE    = 'NOTICE';
    const WARNING   = 'WARNING';
    const ERROR     = 'ERROR';

    protected $debugEnabled;
	protected $providers;

	public function __construct(TwAuditProvider $provider = null)
	{
		if (!is_null($provider))
		{
			$this->addProvider($provider);
		}
	}

    public function log($message, $type = self::INFO)
    {
        foreach ($this->getProviders() as $provider)
        {
            $provider->log($message, $type);

            if ($this->isDebugEnabled())
            {
                print_r('[' . TwTime::getDateTimeMs() . ' ] [' . $type . '] ' . $message . PHP_EOL);
            }
        }
    }

	protected function getProviders()
	{
		return $this->providers;
	}

    public function addProvider(TwAuditProvider $provider)
    {
        $this->providers[]  = $provider;

        return $this;
    }

    public function enableDebug()
    {
        $this->debugEnabled = TRUE;

        return $this;
    }

    public function disableDebug()
    {
        $this->debugEnabled = FALSE;

        return $this;
    }

    public function isDebugEnabled()
    {
        return $this->debugEnabled;
    }
}