<?php

class TwAuditMySqlProvider extends TwAuditProvider
{
	protected $mySqlPdo;
	
	public function __construct(TwMySqlPdo $mySqlPdo, TwConfig $config)
	{
		$this->mySqlPdo	= $mySqlPdo;
		$this->config	= $config;
	}
	
	public function log($message, $type)
	{
		$created_at	= TwTime::getDateTime();
	
		$query	= "INSERT INTO `{$this->getMySqlPdo()->getDatabase()}`.`{$this->getConfig()->get('table')}` SET `created_at` = '{$created_at}', `message` = '{$message}', `type` = '{$type}';";
		$this->getMySqlPdo()->run($query);
	}
	
	protected function getMySqlPdo()
	{
		return $this->mySqlPdo;
	}
	
	protected function getConfig()
	{
		return $this->config;
	}
}