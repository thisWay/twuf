<?php

/**
 * @project 18thRecipes
 * @author  Andrei Hrubaru
 * Date: 12/5/2014
 */
class TwBatch
{
    protected $entities         = array();
    protected $entitiesInTotal  = 0;

    public function addEntity(TwEntity $entity)
    {
        $this->entities[]   = $entity;

        return $this;
    }

    public function setEntities($entities)
    {
        $this->entities = $entities;

        return $this;
    }

    public function getEntities()
    {
        return $this->entities;
    }

    public function getNumberOfEntities()
    {
        return count($this->entities);
    }

    public function getTotalNumberOfEntities()
    {
        return $this->entitiesInTotal;
    }

    public function setTotalNumberOfEntities($number)
    {
        $this->entitiesInTotal  = $number;

        return $this;
    }
}
