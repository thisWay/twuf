<?php

class TwBootstrap
{
    static function getClasses($sources = array())
    {
        $classes    = array();
        $sources    = (array)$sources;

        foreach ($sources as $source)
        {
            $dirInfo    = pathinfo($source);

            if (in_array($dirInfo['basename'], array('.git', '.idea')))
            {
                continue;
            }

            if (is_dir($source))
            {
                if ($directory = opendir($source))
                {
                    while ($file = readdir($directory))
                    {
                        $info   = pathinfo($file);

                        if (isset($info['extension']) && 'php' == $info['extension'])
                        {
                            $class  = str_replace('.class', '', $info['filename']);
                            $classes[$class]    = $source . $info['basename'];
                        }

                        if (is_dir($source . $info['basename']) && $info['basename'] != '.' && $info['basename'] != '..')
                        {
                            $classes    += self::getClasses($source . $info['basename'] . DIRECTORY_SEPARATOR);
                        }
                    }

                    closedir($directory);
                }
            }
        }

        asort($classes);

        return $classes;
    }

    static function generateClassesFile($autoLoadSourcesList, $autoLoadOutputFile)
    {
        $file   = fopen($autoLoadOutputFile, 'w');

        fwrite($file, serialize(self::getClasses($autoLoadSourcesList)));
        fclose($file);
    }

    static function generateConfigFile($configSourceFile, $configOutputFile)
    {
        $file   = fopen($configOutputFile, 'w');

        $config = require_once($configSourceFile);

        // protect from empty config file
        $config = is_array($config) ? $config : array();

        foreach ($config as $name => $value)
        {
            if (is_string($value))
            {
                $reference  = substr($value, 1, strlen($value) -2);

                if (isset($config[$reference]))
                {
                    $config[$name]  = $config[$reference];
                }
            }
        }

        fwrite($file, '<?php return ' . var_export($config, TRUE) . ';');
        fclose($file);

        return $config;
   }
}
