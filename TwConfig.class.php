<?php

class TwConfig
{
    protected $config;
    protected $breadcrumbs;

    public function __construct($config = array(), $breadcrumbs = array('parent' => '', 'location' => ''))
    {
        $this->config       = $config;
        $this->breadcrumbs  = $breadcrumbs;
    }

    public function set($name, $value)
    {
        $this->config[$name]	= $value;

        return $this;
    }


    public function getSubConfig($configBranch, $keepRoot = FALSE)
    {
        $config         = array();
        $branchLength   = strlen($configBranch);

        foreach ($this->config as $key => $value)
        {
            if ($configBranch == substr($key, 0, $branchLength))
            {
                $config[$keepRoot ? $key : substr($key, $branchLength + 1)]	= $value;
            }
        }

        return new TwConfig($config, array('parent' => $this->breadcrumbs['parent'] . $this->breadcrumbs['location'], 'location' => $configBranch));
    }

    public function get($name, $default = null)
    {
        if (isset($this->config[$name]))
        {
            return $this->config[$name];
        }
        else
        {
            trigger_error('Configuration not found for key ' . $name, E_USER_WARNING);
        }

        return $default;
    }

	public function copy($name, TwConfig $config)
	{
		$this->set($name, $config->get($name));

        return $this;
	}

    public function mergeConfig(TwConfig $config)
    {
        foreach ($config->toArray() as $name => $value)
        {
            $this->set($name, $value);
        }

        return $this;
    }


    public function toArray()
    {
        $config = array();

        foreach ($this->config as $name => $value)
        {
            $segments   = explode('.', $name);
            $branch     = $segments[0];
            $levels     = count($segments);

            if (1 == $levels)
            {
                $data   = array(
                    $name   => $value,
                );
            }
            else
            {
                ${'level_' . ($levels - 1)}  = array(
                    $segments[$levels - 1]  => $value,
                );

                for ($i =  $levels - 2; $i > 0; $i--)
                {
                    ${'level_' . $i}  = array(
                        $segments[$i]   => ${'level_' . ($i + 1)},
                    );
                }

                $data   = array(
                    $branch => $level_1,
                );
            }

            $config = array_merge_recursive($config, $data);
        }

        return $config;
    }

    public function toJavaScript()
    {
        return json_encode($this->toArray());
    }
}
