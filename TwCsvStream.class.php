<?php

class TwCsvStream
{
    static function getCsvAsArray($file, $skipRows = 0, $columns = null)
    {
        if (file_exists($file))
        {
            $file       = fopen($file, 'r');
            $rows       = array();
            $rowIndex   = 0;

            while( $row = fgetcsv($file))
            {
                if ($rowIndex >= $skipRows)
                {
                    $rows[]	= self::mapToColumns($row, $columns);
                }

                $rowIndex++;
            }

            fclose($file);

            return $rows;
        }
        else
        {
            throw new Exception('The supplied CSV file ' . $file . ' was not found!');
        }
    }

    protected static function mapToColumns($row, $columns = null)
    {
        if (is_null($columns))
        {
            return $row;
        }

        $result = array();
        $index  = 0;

        foreach ($columns as $column)
        {
            if (!is_null($column))
            {
                $result[$column]    = $row[$index];
            }

            $index++;
        }

        return $result;
    }
}
