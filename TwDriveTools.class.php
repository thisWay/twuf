<?php

class TwDriveTools
{
    static function getFiles($source, $recursive = TRUE, $extensions = array(), $skipDirectories = array(), $skipExtensions = array())
    {
//        print_r('running the getFiles for directory ' . $source . PHP_EOL);

        $skipDirectories    = array_merge(array('.', '..'), $skipDirectories);
        $skipExtensions     = array_merge(array(''), $skipExtensions);
        $files              = array();

        if (is_dir($source))
        {
            if ($directory = opendir($source))
            {
                while ($file = readdir($directory))
                {
                    $info   = pathinfo($file);

                    //print_r($info);

                    if ($recursive && is_dir($source . $info['basename']) && !in_array($info['basename'], $skipDirectories))
                    {
//                        print_r('files before ');
//                        print_r($files);


                        $files  = array_merge($files, self::getFiles($source . $info['basename'] . DIRECTORY_SEPARATOR, $recursive, $extensions, $skipDirectories, $skipExtensions));

//                        print_r('files after ');
//                        print_r($files);
                    }

                    if (isset($info['extension']) && !in_array($info['extension'], $skipExtensions) && (count($extensions) ? in_array($info['extension'], $extensions) : true))
                    {
                        $files[]    = $source . $info['basename'];

//                        $class  = str_replace('.class', '', $info['filename']);
//                        $classes[$class]    = $source . $info['basename'];
                    }

                }

                closedir($directory);
            }
        }

//        print_r('these are the files for ' . $source);
//        print_r($files);

        return $files;
    }
}
