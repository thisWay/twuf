<?php

class TwFileStream
{
    static function getRowsAsArray($file, $skipRows = 0, $columns = null)
    {
        if (file_exists($file))
        {
            $file       = fopen($file, 'r');
            $rows       = array();

            while( $row = fgets($file))
            {
                $rows[] = $row;
            }

            fclose($file);

            return $rows;
        }
        else
        {
            throw new Exception('The supplied file ' . $file . ' was not found!');
        }
    }
}
