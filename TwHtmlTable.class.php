<?php

class TwHtmlTable
{
    protected $columns          = array();
    protected $labels           = array();
    protected $isSortable       = FALSE;
    protected $sortableColumns  = array();
    protected $sortColumn;
    protected $sortDirection        = 'asc';
    protected $wrapperTemplate      = '<table class="table table-striped table-bordered table-hover">{CONTENT}</table>';
    protected $thTemplate           = '<th>{LABEL}</th>';
    protected $sortableThTemplate   = '<th><a href="{URL}">{LABEL} {SORT}</a></th>';
    protected $sortAscTemplate      = '<span class="glyphicon glyphicon-chevron-down"></span>';
    protected $sortDescTemplate     = '<span class="glyphicon glyphicon-chevron-up"></span>';
    protected $urlTemplate          = '?page=1&sort={SORT_COLUMN};{SORT_DIRECTION}';

    public function __construct($columns = null)
    {
        if (!is_null($columns))
        {
            $this->setColumns($columns);
        }
    }

    public function setColumns($columns)
    {
        $this->columns  = (array)$columns;

        return $this;
    }

    public function enableSorting($columns = null)
    {
        if (is_null($columns) && is_null($this->sortableColumns))
        {
            $this->sortableColumns  = array_keys($this->columns);
        }

        if (!is_null($columns))
        {
            foreach ($columns as $column)
            {
                if (isset($this->columns[$column]))
                {
                    $this->sortableColumns  = $columns;
                }
                else
                {
                    throw new Exception(' The column "' . $column . '" you are trying to allow sorting on does not exist!');
                }
            }
        }

        $this->isSortable   = TRUE;

        return $this;
    }

    public function disableSorting()
    {
        $this->isSortable   = FALSE;

        return $this;
    }

    public function setUrlTemplate($template)
    {
        $this->urlTemplate  = $template;

        return $this;
    }

    public function setSortColumn($column, $direction = 'asc')
    {
        if (in_array($column, $this->sortableColumns))
        {
            $this->sortColumn       = $column;
            $this->sortDirection    = $direction == 'asc' ? 'asc' : 'desc';
        }
        else
        {
            throw new Exception('The column "' . $column . '" you are trying to sort on is not sortable!');
        }

        return $this;
    }

    public function setDataSources($dataSources)
    {

        return $this;
    }

    public function getTableHead()
    {
        $head   = '<thead>';

        foreach ($this->columns as $id => $label)
        {
            if ($this->isSortable && in_array($id, $this->sortableColumns))
            {
                $direction  = ($id != $this->sortColumn) ? 'asc' : ('asc' == $this->sortDirection ? 'desc' : 'asc');
                $url        = str_replace(array('{SORT_COLUMN}', '{SORT_DIRECTION}'), array( urlencode($id), $direction), $this->urlTemplate);

                if ($id == $this->sortColumn)
                {
                    $sort   = ('asc' == $this->sortDirection) ? $this->sortAscTemplate : $this->sortDescTemplate;
                }
                else
                {
                    $sort   = '';
                }

                $th         = str_replace(array('{URL}', '{LABEL}', '{SORT}'), array($url, $label, $sort), $this->sortableThTemplate);
            }
            else
            {
                $th         = str_replace('{LABEL}', $label, $this->thTemplate);
            }

            $head   .= $th . PHP_EOL;
        }

        return $head . '</thead>';
    }


    public function __toString()
    {
        $output = '';

        $output = $this->getTableHead();


        return str_replace('{CONTENT}', $output, $this->wrapperTemplate);
    }
}
