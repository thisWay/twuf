<?php

class TwHttpClient
{
	private $curl;
	private $timeout	= 1000;

	public function __construct()
	{
		$this->curl = curl_init();

		curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($this->curl, CURLOPT_TIMEOUT, $this->timeout);
	}

	public function post($url, $parameters)
	{
        die('this is a POST request');

        curl_setopt($this->curl, CURLOPT_URL, $url);
		curl_setopt($this->curl, CURLOPT_POST, TRUE);
		curl_setopt($this->curl, CURLOPT_POSTFIELDS, $parameters);

		$response = curl_exec($this->curl);
		$headers = curl_getinfo($this->curl);

//		curl_close($this->curl);

		return $response;
	}

    public function get($url, $parameters)
    {
        $url    .= '?' . http_build_query($parameters);

        die('this is a GET request to ' . $url);

        curl_setopt($this->curl, CURLOPT_URL, $url);
        curl_setopt($this->curl, CURLOPT_HTTPGET, TRUE);

        $response = curl_exec($this->curl);

        return $response;
    }



	public function downloadToFile($url, $path)
	{
		curl_setopt($this->curl, CURLOPT_URL, $url);
		curl_setopt($this->curl, CURLOPT_HEADER, FALSE);
		curl_setopt($this->curl, CURLOPT_FOLLOWLOCATION, TRUE);
		curl_setopt($this->curl, CURLOPT_BINARYTRANSFER, TRUE);
		curl_setopt($this->curl, CURLOPT_HTTPGET, TRUE);

		$bytes	= curl_exec($this->curl);

//		curl_close($this->curl);

		$file = fopen($path, 'w');
		fwrite($file, $bytes);
		fclose($file);
	}

    public function requestJson($url, $parameters = null)
    {
        return json_decode($this->post($url, $parameters));
    }
}