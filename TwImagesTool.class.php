<?php

class TwImagesTool
{
    const TOP_LEFT_CORNER       = 1;
    const TOP_RIGHT_CORNER      = 2;
    const BOTTOM_LEFT_CORNER    = 3;
    const BOTTOM_RIGHT_CORNER   = 4;

    static function addPadding($imagePath, $top, $right, $bottom, $left, $color, $paddedImagePath = null)
    {
        if (is_null($paddedImagePath))
        {
            $paddedImagePath    = $imagePath;
        }

        $image          = imagecreatefromstring(file_get_contents($imagePath));
        list($imageWidth, $imageHeight) = getimagesize($imagePath);
        $canvasWidth    = $imageWidth + $right + $left;
        $canvasHeight   = $imageHeight + $top + $bottom;
        $canvas         = imagecreatetruecolor($canvasWidth, $canvasHeight);
        $bgColor        = imagecolorallocate($canvas, $color[0], $color[1], $color[2]);

        imagefill($canvas, 0, 0, $bgColor);
        imagecopy($canvas, $image, $left, $top, 0, 0, $imageWidth, $imageHeight);
        imagejpeg($canvas, $paddedImagePath, 80);
        imagedestroy($image);
        imagedestroy($canvas);
    }

    static function addText($imagePath, $text, $size, $fontPath, $posX, $posY, $color, $writtenImagePath = null, $relativeTo = self::TOP_LEFT_CORNER)
    {
        if (is_null($writtenImagePath))
        {
            $writtenImagePath   = $imagePath;
        }

        $image  = imagecreatefromstring(file_get_contents($imagePath));
        $color  = imagecolorallocate($image, $color[0], $color[1], $color[2]);
        list($imageWidth, $imageHeight) = getimagesize($imagePath);

        switch($relativeTo)
        {
            case self::BOTTOM_RIGHT_CORNER :
                $posX   = $imageWidth - $posX;
                $posY   = $imageHeight - $posY;
                break;

            case self::BOTTOM_LEFT_CORNER :
                $posY   = $imageHeight - $posY;
                break;

            case self::TOP_RIGHT_CORNER :
                $posX   = $imageWidth - $posX;
                break;
        }

        imagettftext($image, $size, 0, $posX, $posY, $color, $fontPath, $text);
        imagejpeg($image, $writtenImagePath, 80);
        imagedestroy($image);
    }
}