<?php

/**
 * Class TwMySqlMixedRepository
 * This class allows grouping the results of multiple TwQuery in one single TwBatch for UNION queries
 */
class TwMySqlMixedRepository extends TwMySqlRepository
{
    /** @var TwMySqlRepository[] $repositories */
    protected $repositories = array();
    /** @var TwQuery[] $queries */
    protected $queries  = array();

    public function init(){
        $this->setTableName('mixed');
    }

    public function addRepository(TwMySqlRepository $repository, $entityName = null)
    {
        if (is_null($entityName))
        {
            $entityName = $repository->getEntityName();
        }

        $this->repositories[$entityName]    = $repository;
        $this->setMySqlPdo($repository->getMySqlPdo());

        return $this;
    }

    public function addQuery(TwQuery $query, $entityName = null)
    {
        if (is_null($entityName))
        {
            $entityName = $query->guessEntityName();
        }

        $this->queries[$entityName] = $query;

        return $this;
    }




    /**
     * Returns a batch of the entities this repository contains matching the supplied query (eg: paginated listings)
     * @param int $batchNumber
     * @param int $batchSize
     * @return TwBatch
     * @throws Exception
     */
    public function getBatchByQuery(TwQuery $query)
    {
        foreach ($this->repositories as $entityName => $repository)
        {
            $columns    = $this->queries[$entityName]->getColumns();
            $columns[]  = '"' . $entityName . '" AS __entity__';
            $queries[]  = (string)$this->queries[$entityName]->setColumns($columns);
        }

        $from   = '((' .implode(') UNION ALL (',$queries) . ')) AS ' . $this->getTableName();

        $query->setFrom($from);
        $query->enableSmartCount();

//        print_r($query . PHP_EOL);

        // run the query on the last repository PDO connection
        $result     = $this->getMySqlPdo()->run($query);
        $batch      = new TwBatch();

        foreach ($result as $row) {
            $entityName = $row['__entity__'];
            unset($row['__entity__']);
            $batch->addEntity(new $entityName($row));
        }

        $batch->setTotalNumberOfEntities($this->getFoundRows());

        return $batch;
    }



}
