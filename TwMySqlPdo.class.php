<?php

class TwMySqlPdo
{
    protected $username;
    protected $password;
    protected $host;
    protected $port;
    protected $database;

    protected $pdo;
    protected $driver   = 'mysql';

    public function __construct(TwConfig $config)
    {
        foreach ($config->toArray() as $property => $value)
        {
            if (property_exists($this, $property))
            {
                $this->$property = $value;
            }
        }
    }


    /**
     * Returns one result as an instance of the class with properties set from the row columns
     * @param $query
     * @param array $queryParameters
     * @param string $class
     * @return bool|TwEntity
     */
    public function fetchOneAsObject($query, $queryParameters = array(), $class = 'stdClass')
    {
//        print_r('running query: ' . $query . PHP_EOL);
//        print_r($queryParameters);

        $statement  = $this->execute($query, $queryParameters);

        return $statement->fetchObject($class);
    }

    public function insert($sql, $parameters = array())
    {
        $statement  = $this->execute($sql, $parameters);

        return $this->getPdo()->lastInsertId();
    }


    /**
     * @param $query
     * @param array $parameters
     * @return PDOStatement
     * @throws Exception
     */
    public function execute($query, $parameters = array())
    {
        $statement  = $this->getPdo()->prepare($query);
        $statement->execute($parameters);

        if ($statement->errorCode() != '00000')
        {
            $ansiErrorCode      = $statement->errorInfo()[0];
            $mySqlErrorCode     = $statement->errorInfo()[1];
            $mySqlErrorMessage  = $statement->errorInfo()[2];

            throw new Exception(sprintf('(%s) %s', $ansiErrorCode, $mySqlErrorMessage), $mySqlErrorCode);
        }

        return $statement;
    }


    /**
     * @param $query
     * @param array $parameters
     * @param string $class
     * @return TwObject[]|bool
     */
    public function fetchAllAsObjects($query, $parameters = array(), $class = 'stdClass', $listKey = null)
    {
        $statement  = $this->execute($query, $parameters);
        $records    = $statement->fetchAll();

        if ($records)
        {
            $i  = 0;
            $entities   = array();

            foreach ($records as $record)
            {
                $key    = !is_null($listKey) ? $record[$listKey] : $i++;
                /** @var TwEntity $entity */
                $entity     = new $class;
                $entity->copyAttributesFromArray($record);
                $entities[$key] = $entity;
            }

            return $entities;
        }

        return FALSE;
    }


    public function quote($value)
    {
        return $this->getPdo()->quote($value);
    }



	public function run($query, $arguments = array())
	{
//		$result = $this->getPdo()->query($query);
        $result = $this->getPdo()->prepare($query);

/*        print_r('error info after prepare' . PHP_EOL);
        print_r($result->errorInfo());
        print_r($this->getPdo()->errorInfo());
*/

        $result->execute($arguments);

//        print_r('error info after execute' . PHP_EOL);

        if ($result->errorCode() != '00000')
        {
            $ansiErrorCode      = $result->errorInfo()[0];
            $mySqlErrorCode     = $result->errorInfo()[1];
            $mySqlErrorMessage  = $result->errorInfo()[2];

            throw new Exception(sprintf('(%s) %s', $ansiErrorCode, $mySqlErrorMessage), $mySqlErrorCode);
        }


//        print_r($result->errorInfo());
//        print_r($this->getPdo()->errorInfo());


        $result = $result->fetchAll();

//        print_r('just ran a fetchAll' . PHP_EOL);
//        var_dump($result);

        if (method_exists($result, 'errorInfo'))
        {
            print_r($result->errorInfo());
        }
        else
        {
//            echo 'errorInfo not set' . PHP_EOL;
        }

//        print_r($this->getPdo()->errorInfo());
//        print_r('just ran a fetchAll - end' . PHP_EOL);

//        die(__METHOD__ . ' * ' . __LINE__);

		if (!$result)
		{
//			print_r($this->getPdo()->errorInfo());
		}

        return $result;
	}

    public function getLastInsertId()
    {
        return $this->getPdo()->lastInsertId();
    }

    protected  function getPdo()
    {
        if (!($this->pdo instanceof PDO))
        {
            $this->pdo = new PDO($this->getDsn(), $this->username, $this->password);
            $this->pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
            $this->pdo->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, 'SET NAMES utf8;');
        }

        return $this->pdo;
    }

    /**
     * Returns the database name
     * @return string
     */
    public function getDatabase()
    {
        return $this->database;
    }

    /**
     * Returns the PDO DSN connection string
     * @return string
     */
    public function getDsn()
    {
        return $this->driver . ':dbname=' . $this->getDatabase() . ';host=' . $this->host;
    }
}
