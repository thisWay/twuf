<?php

abstract class TwMySqlRepository
{
    /** @var TwMySqlPdo */
    protected $mySqlPdo;
    protected $tableName;
    protected $primaryKey;
    protected $entityName;

    /**
     * Creates the repository instance and configures the MySQL PDO instance to use
     * @param TwMySqlPdo $mySqlPdo
     */
    public function __construct(TwMySqlPdo $mySqlPdo = null)
    {
        if (!is_null($mySqlPdo))
        {
            $this->setMySqlPdo($mySqlPdo);
        }

        $this->init();
    }

    /**
     * The init() method should be used for setting the table name and the primary key etc.
     * @return mixed
     */
    protected abstract function init();

    /**
     * Configures the table name of the data source for the repository
     * At the same time is entity name is guessed based on a convention
     * @param $tableName
     * @return $this
     */
    protected function setTableName($tableName)
    {
        $this->tableName    = (string)$tableName;
        $this->setEntityName(TwStringsTool::snakeCasedToCamelCased($tableName, true));

        return $this;
    }

    /**
     * Returns the table name of the data source for the repository
     * @return string
     * @throws Exception
     */
    public function getTableName()
    {
        if (is_null($this->tableName))
        {
            throw new Exception('Did you forget to set the table name for this repository?');
        }

        return $this->tableName;
    }

    /**
     * Configues the primary key for the table this repository is using
     * @param $primaryKey
     * @return $this
     */
    protected function setPrimaryKey($primaryKey)
    {
        $this->primaryKey   = (string)$primaryKey;

        return $this;
    }

    /**
     * Returns the primary key of the table this repository is using
     * @return mixed
     */
    public function getPrimaryKey()
    {
        return $this->primaryKey;
    }

    /**
     * Returns the name of the database the repository is using
     * @return string
     */
    public function getDatabaseName()
    {
        return $this->getMySqlPdo()->getDatabase();
    }

    /**
     * Returns the database name concatenated with the table name to facilitate query construction
     * @return string
     * @throws Exception
     */
    public function getContainer()
    {
        return "`{$this->getDatabaseName()}`.`{$this->getTableName()}`";
    }

    /**
     * Configures the name of the entities that this repository stores
     * @param $entityName
     * @return $this
     */
    public function setEntityName($entityName)
    {
        $this->entityName   = (string)$entityName;

        return $this;
    }

    /**
     * Returns the name of the entities that this repository stores
     * @return string
     */
    public function getEntityName()
    {
        return $this->entityName;
    }

    /**
     * Configures the MySQL PDO instance to be used by the repository
     * @param TwMySqlPdo $mySqlPdo
     * @return $this
     */
    protected function setMySqlPdo(TwMySqlPdo $mySqlPdo)
    {
        $this->mySqlPdo = $mySqlPdo;

        return $this;
    }

    /**
     * Returns the MySQL PDO instance used by the repository
     * @return TwMySqlPdo
     */
    protected function getMySqlPdo()
    {
        return $this->mySqlPdo;
    }





    /**
     * Returns a batch of the entities this repository contains matching the supplied query (eg: paginated listings)
     * @param int $batchNumber
     * @param int $batchSize
     * @return TwBatch
     * @throws Exception
     */
    public function getBatchByQuery(TwQuery $query)
    {
        print_r($query . PHP_EOL);

        $query->enableSmartCount();
        $result     = $this->getMySqlPdo()->run($query);
        $batch      = new TwBatch();
        $entiyName  = $this->getEntityName();

        foreach ($result as $row) {
            $batch->addEntity(new $entiyName($row));
        }

        $batch->setTotalNumberOfEntities($this->getFoundRows());

        return $batch;
    }



    /**
     * Returns a specific configurable size batch of the entities this repository contains (eg: backend paginated listings)
     * @param int $batchNumber
     * @param int $batchSize
     * @return TwBatch
     * @throws Exception
     */
    public function getBatch($batchNumber = 1, $batchSize = 20, $orderBy = null, $orderType = 'ASC')
    {
        $batchNumber    = max((int)$batchNumber, 1);

        if (is_null($orderBy))
        {
            $orderBy    = $this->getPrimaryKey();
        }

        $sql    = 'SELECT SQL_CALC_FOUND_ROWS * FROM ' . $this->getContainer() . ' ORDER BY ' . $orderBy . ' ' . strtoupper($orderType) . ' LIMIT ' . (int)$batchSize . ' OFFSET ' . ($batchNumber - 1) * $batchSize . ';';

        $result     = $this->getMySqlPdo()->run($sql);
        $batch      = new TwBatch();
        $entiyName  = $this->getEntityName();

        foreach ($result as $row) {
            $batch->addEntity(new $entiyName($row));
        }

        $batch->setTotalNumberOfEntities($this->getFoundRows());

        return $batch;
    }

    /**
     * Returns the number of rows the last query would have fetched without a LIMIT clause
     */
    public function getFoundRows()
    {
        $sql        = 'SELECT FOUND_ROWS() AS found_rows;';
        $result     = $this->getMySqlPdo()->run($sql);

        return $result[0]['found_rows'];
    }

    /**
     * Returns the number of all records in the table with no deleted_at set
     * @return int
     * @throws Exception
     */
    public function getActiveEntitiesCount()
    {
        $query  = new TwQuery();
        $query
            ->setFrom($this->getContainer())
            ->setColumns(array('count' => 'COUNT(1)'))
            ->setWhere('!deleted_at');

        $result     = $this->getMySqlPdo()->run($query);

        return $result[0]['count'];
    }

    public function findOneByPrimaryKey($primaryKey)
    {
        $query  = new TwQuery();
        $query
            ->setFrom($this->getContainer())
            ->setWhere($this->getPrimaryKey() . ' = :' . $this->getPrimaryKey());

        $parameters = array(
            ':' . $this->getPrimaryKey()    => $primaryKey,
        );

        return $this->getMySqlPdo()->fetchOneAsObject($query, $parameters, $this->getEntityName());
    }

    public function findAllActiveByPrimaryKeys($primaryKeys)
    {
        foreach ($primaryKeys as $index => $primaryKey)
        {
            $primaryKeys[$index]    = $this->getMySqlPdo()->quote($primaryKey);
        }

        $query  = new TwQuery();
        $query
            ->setFrom($this->getContainer())
            ->setWhere($this->getPrimaryKey() . ' IN (' . implode(', ', $primaryKeys) . ')');

        return $this->getMySqlPdo()->fetchAllAsObjects($query, null, $this->getEntityName());
    }

    /**
     * Inserts a new record into the repository and returns the new autoincrement ID
     * @param TwEntity $item
     * @return string
     * @throws Exception
     */
    public function add(TwEntity $item)
    {
        if (!count($item->listAttributes()))
        {
            throw new Exception('To use this functionality you must define at least one attribute for your object.');
        }

        $sql    = 'INSERT INTO ' . $this->getContainer() . ' SET ';

        $set        = array();
        $parameters = array();

        foreach ($item->listAttributes() as $column)
        {
            $method = 'get' . TwStringsTool::snakeCasedToCamelCased($column, TRUE);
            $column = TwStringsTool::camelCasedToSnakeCased($column);

//            print_r($method . PHP_EOL);

            $value  = $item->{$method}();

            if (!is_null($value))
            {
                $set[]                      = "`{$column}` = :{$column}";
                $parameters[':' . $column]  = $value;
            }
        }

        $sql    .= implode(', ', $set) . ';';

//        print_r($sql);
//        print_r($parameters);

        return $this->getMySqlPdo()->insert($sql, $parameters);
    }

    //TODO: make this work with primary keys
    public function update(TwEntity $entity)
    {
        if (!count($entity->listAttributes()))
        {
            throw new Exception('To use this functionality you must define at least one attribute for your object.');
        }

        $sql    = 'UPDATE ' . $this->getContainer() . ' SET ';

        $set        = array();
        $parameters = array(
            ':id'   => $entity->getId()
        );
        $where      = array(
            'id = :id'
        );

        foreach ($entity->listAttributes() as $column)
        {

            $method = 'get' . TwStringsTool::snakeCasedToCamelCased($column, TRUE);
            $column = TwStringsTool::camelCasedToSnakeCased($column);

            $value  = $entity->{$method}();

            if (!is_null($value))
            {
                $set[]                      = "`{$column}` = :{$column}";
                $parameters[':' . $column]  = $value;
            }
        }

        $sql    = 'UPDATE ' . $this->getContainer() . ' SET ' . implode(', ', $set) . ' WHERE ' . implode(', ', $where) . ';';

//        print_r($sql);
//        print_r($parameters);

        return $this->getMySqlPdo()->execute($sql, $parameters);
    }
}
