<?php

class TwNavigation
{
    protected $tree = array();

    public function __construct(TwConfig $config = null)
    {
        if (!is_null($config))
        {
            $this->loadConfig($config);
        }
    }

    public function loadConfig(TwConfig $config)
    {
        foreach ($config->toArray() as $setting => $value)
        {
            if (isset($value['title']))
            {
                $this->tree[$setting]   = $value;
            }
        }

        return $this;
    }

    public function getTree()
    {
        return $this->tree;
    }
}
