<?php

class TwObject
{
    protected $mapping          = array();
    protected $attributes       = array();
    protected $attributeValues  = array();

    /**
     *
     * @param null|array $attributes
     */
    public function __construct($attributes = null)
    {
        if (!is_null($attributes))
        {
            $this->copyAttributesFromArray($attributes);
        }
    }

    /**
     * Converts any array into an instance of stdClass using keys as attributes
     * @param $array
     * @return StdClass
     */
    static function arrayToObject($array)
    {
        $object = new StdClass();

        foreach ($array as $key => $value)
        {
            $object->{$key}	= $value;
        }

        return $object;
    }

    /**
     * Converts the keys of an array into it's own attributes
     * @param $array
     * @return TwObject
     */
    public function copyAttributesFromArray($array)
    {
        return $this->copyAttributesFromObject(self::arrayToObject((array)$array));
    }

    /**
     * Converts the public attributes of another object into it's own attributes
     * @param $source
     * @return $this
     */
    public function copyAttributesFromObject($source)
    {
        // flip the attributes array to allow isset() operations on it
        $availableAttributes        = array_flip($this->attributes);
        $sourceAttributes           = get_object_vars($source);

        foreach ($sourceAttributes as $sourceAttributeName => $sourceAttributeValue)
        {
            // store the initial source values for name and value to be used if no callbacks or renamings are performed
            $attributeName  = $sourceAttributeName;
            $attributeValue = $sourceAttributeValue;

            // check for mappings that translate into modifying an input attribute
            if (isset($this->mapping[$sourceAttributeName]))
            {
                // operate the callback modifiers first
                foreach ((array)$this->mapping[$sourceAttributeName] as $modifier)
                {
                    if (strrpos($modifier, '()'))
                    {
                        $callbackMethodName = substr($modifier, 0, strlen($modifier) - 2);
                        $attributeValue     = $this->{$callbackMethodName}($sourceAttributeValue);
                    }
                }

                // operate the renaming modifiers second
                foreach ((array)$this->mapping[$sourceAttributeName] as $modifier)
                {
                    if (!strrpos($modifier, '()'))
                    {
                        $attributeName  = $modifier;
                        // create a copy of the input attribute with the mapped output name
                        $sourceAttributes[$attributeName]   = $attributeValue;
                        // remove the original input attribute
                        unset($sourceAttributes[$sourceAttributeName]);
                    }
                }
            }

            // normalise attribute name
            $normalisedAttributeName    = TwStringsTool::snakeCasedToCamelCased($attributeName);
            // check for a custom setter
            $customSetterMethodName     = 'set' . ucfirst($normalisedAttributeName);

            // try to run a setter method on the attribute if it exists else save the value in the attribute
            if (isset($availableAttributes[$normalisedAttributeName]))
            {
                if (method_exists($this, $customSetterMethodName))
                {
                    $this->{$customSetterMethodName}($attributeValue);
                    continue;
                }
                else
                {
                    $this->attributeValues[$normalisedAttributeName]    = $attributeValue;
                }
            }
        }
    }

    /**
     * Transforms any value to NULL (generic callback method)
     * @param $value
     * @return null
     */
    public function nullify($value)
    {
        return null;
    }

    public function __set($name, $value)
    {
//        print_r('running magical __set on ' . $name . PHP_EOL);

        // check if we are trying to set a property that has exactly the same name as an accepted attribute
        if (in_array($name, $this->attributes)) {
            $this->attributeValues[$name] = $value;

            return $this;
        }

        // check if we are trying to set an accepted attribute directly from a MySQL table (eg: PDO fetchObject)
        $attributeName  = TwStringsTool::snakeCasedToCamelCased($name);
        if (in_array($attributeName, $this->attributes)) {
            $this->attributeValues[$attributeName] = $value;

            return $this;
        }
    }

    /**
     * Used by the Repository class to build the SQLs
     * @return array
     */
    public function listAttributes()
    {
        return $this->attributes;
    }
























    public function trySetterThroughMapping($property, $value)
    {
        $targetMapping      = static::$mapping[$property];
        $targetAttribute    = is_array($targetMapping) ? $targetMapping['attribute'] : $targetMapping;

        if (isset($targetMapping['callback']) && method_exists($this, $targetMapping['callback']))
        {
            $this->{$targetMapping['callback']}($value);

            return TRUE;
        }
        elseif (property_exists($this, $targetAttribute))
        {
            $this->{$targetAttribute}   = $value;

            return TRUE;
        }

        return FALSE;
    }

    public function tryGetterThroughMapping($property)
    {
        $targetMapping      = static::$mapping[$property];
        $targetAttribute    = is_array($targetMapping) ? $targetMapping['attribute'] : $targetMapping;

        if (property_exists($this, $targetAttribute))
        {
            return $this->{$targetAttribute};
        }
    }



    public function __call($method, $arguments)
    {
        if ('set' == substr($method, 0, 3))
        {
            $attributeName  = lcfirst(substr($method, 3));

            // check if we have an attribute with this name
            if (in_array($attributeName, $this->attributes))
            {
                $this->attributeValues[$attributeName]  = $arguments[0];

                return $this;
            }
        }

        if ('get' == substr($method, 0, 3))
        {
            $attributeName  = lcfirst(substr($method, 3));

//            var_dump($attributeName);

            // check if we have a value slot for this attribute name
            if (array_key_exists($attributeName, $this->attributeValues))
            {
                return $this->attributeValues[$attributeName];
            }

/*            if (isset(static::$mapping[$property]))
            {
                return $this->tryGetterThroughMapping($property, $arguments[0]);
            }
        }

        throw new Exception(sprintf('Neither the "%s" method or the "%s" property exist on this "%s" class.', $method, $property, __CLASS__));
*/
            return null;
        }
    }
















	static function fromArray($array)
	{
        trigger_error(__METHOD__ . ' still used', E_USER_ERROR);

		$object	= new StdClass();

		foreach ($array as $key => $value)
		{
			$object->{$key}	= $value;
		}

		return $object;
	}
}
