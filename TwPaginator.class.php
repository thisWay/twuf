<?php

class TwPaginator
{
    protected $pageSize;
    protected $itemsCount;
    protected $currentPage              = 1;
    protected $wrapperTemplate          = '<nav><ul class="pager">{CONTENT}</ul></nav>';
    protected $urlTemplate              = '?page={PAGE_NUMBER}';
    protected $pageTemplate             = '<li><a href="{URL}">{PAGE_NUMBER}</a></li>';
    protected $activePageTemplate       = '<li class="active"><a href="{URL}">{PAGE_NUMBER}</a></li>';
    protected $navigationTemplate       = '<li><a href="{URL}">{LABEL}</a></li>';
    protected $displayNavigational      = TRUE;
    protected $nextLabel                = '&raquo;';
    protected $previousLabel            = '&laquo;';
    protected $idealNumberOfNeighbours  = 0;
    protected $idealNumberOfButtons;

    public function __construct($pageSize, $itemsCount)
    {
        $this->pageSize     = $pageSize;
        $this->itemsCount   = $itemsCount;
    }

    public function setCurrentPage($pageNumber)
    {
        $this->currentPage  = max((int)$pageNumber, 1);

        return $this;
    }

    public function getCurrentPage()
    {
        return $this->currentPage;
    }

    public function setWrapperTemplate($template)
    {
        $this->wrapperTemplate  = $template;

        return $this;
    }

    public function setUrlTemplate($template)
    {
        $this->urlTemplate  = $template;

        return $this;
    }

    public function setPageTemplate($template)
    {
        $this->pageTemplate   = $template;

        return $this;
    }

    public function setActivePageTemplate($template)
    {
        $this->activePageTemplate   = $template;

        return $this;
    }

    public function setNavigationTemplate($template)
    {
        $this->navigationTemplate   = $template;

        return $this;
    }

    public function setNextLabel($label)
    {
        $this->nextLabel    = $label;

        return $this;
    }

    public function setPreviousLabel($label)
    {
        $this->previousLabel    = $label;

        return $this;
    }

    public function enableNavigation()
    {
        $this->displayNavigational  = TRUE;

        return $this;
    }

    public function disableNavigation()
    {
        $this->displayNavigational  = FALSE;

        return $this;
    }

    public function setIdealNumberOfNeighbours($number)
    {
        $this->idealNumberOfNeighbours  = $number;
        $this->idealNumberOfButtons     = $number * 2 + 1;

        return $this;
    }






    public function getNumberOfPages()
    {
        return ceil($this->itemsCount / $this->pageSize);
    }

    public function isFirstPage()
    {
        return 1 === $this->currentPage;
    }

    public function isLastPage()
    {
        return $this->currentPage === $this->getNumberOfPages();
    }



    public function __toString()
    {
        $output = '';

        if (0 === $this->idealNumberOfNeighbours || $this->idealNumberOfButtons >= $this->getNumberOfPages())
        {
            $first  = 1;
            $last   = $this->getNumberOfPages();
        }
        // the starting (left balanced) segment is active
        elseif ($this->currentPage < $this->idealNumberOfNeighbours + 1) {
            $first = 1;
            $last = $this->idealNumberOfButtons;
        }
        // the ending (right balanced) segment is active
        elseif($this->currentPage > $this->getNumberOfPages() - $this->idealNumberOfNeighbours) {
            $first = $this->getNumberOfPages() - $this->idealNumberOfButtons + 1;
            $last = $this->getNumberOfPages();
        }
        // the ideal (balanced) segment is active
        else
        {
            $first  = max($this->currentPage - $this->idealNumberOfNeighbours, 1);
            $last   = min($this->currentPage + $this->idealNumberOfNeighbours, $this->getNumberOfPages());
        }

        for ($i = $first; $i <= $last; $i++)
        {
            $url    = str_replace('{PAGE_NUMBER}', $i, $this->urlTemplate);

            if ($i == $this->currentPage)
            {
                $output .= str_replace(array('{URL}', '{PAGE_NUMBER}'), array($url, $i), $this->activePageTemplate) . PHP_EOL;
            }
            else
            {
                $output .= str_replace(array('{URL}', '{PAGE_NUMBER}'), array($url, $i), $this->pageTemplate) . PHP_EOL;
            }
        }

        if ($last < $this->getNumberOfPages())
        {
//            $output .= str_replace('{PAGE_NUMBER}', '...', $this->pageTemplate) . PHP_EOL;
        }

        if ($this->displayNavigational)
        {
            $previousPageUrl    = str_replace('{PAGE_NUMBER}', $this->currentPage > 1 ? $this->currentPage - 1 : 1, $this->urlTemplate);
            $nextPageUrl        = str_replace('{PAGE_NUMBER}', $this->currentPage < $this->getNumberOfPages() ? $this->currentPage + 1 : $this->getNumberOfPages(), $this->urlTemplate);

            $output = str_replace(array('{URL}', '{LABEL}'), array($previousPageUrl, $this->previousLabel), $this->navigationTemplate) . PHP_EOL . $output;
            $output .= str_replace(array('{URL}', '{LABEL}'), array($nextPageUrl, $this->nextLabel), $this->navigationTemplate) . PHP_EOL;
        }

        return str_replace('{CONTENT}', $output, $this->wrapperTemplate);
    }
}
