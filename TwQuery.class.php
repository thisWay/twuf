<?php

class TwQuery
{
    const TYPE_SELECT   = 'select';
    const TYPE_UPDATE   = 'update';
    const TYPE_INSERT   = 'insert';
    const TYPE_DELETE   = 'delete';

    static $syntax = array(
        self::TYPE_SELECT   => 'SELECT {MODIFIERS} {COLUMNS} FROM {FROM} {WHERE} {ORDER_BY} {LIMIT}',
    );

    protected $query;
    protected $columns          = array('*');
    protected $type             = self::TYPE_SELECT;
    protected $from;
    protected $modifiers        = array();
    protected $limit;
    protected $offset           = 0;
    protected $orderColumn;
    protected $orderDirection   = 'ASC';
    protected $where;


    public function __construct($query = null)
    {
        if (!is_null($query))
        {
            $this->setQuery($query);
        }
    }

    public function setQuery($query)
    {
        $this->query    = $query;

        return $this;
    }

    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    public function setColumns($columns)
    {
        $columns    = (array)$columns;

        if (count($columns))
        {
            $this->columns  = $columns;
        }

        return $this;
    }

    public function getColumns()
    {
        return $this->columns;
    }

    public function setFrom($from)
    {
        $this->from = $from;

        return $this;
    }


    public function setWhere($where, $value = null)
    {
        $this->where    = str_replace(array('{VALUE}', '{value}'), $value, $where);

        return $this;
    }

    public function enableSmartCount()
    {
        $this->modifiers['SQL_CALC_FOUND_ROWS'] = TRUE;

        return $this;
    }

    public function setLimit($limit, $offset = null)
    {
        $this->limit    = (int)$limit;

        if (!is_null($offset))
        {
            $this->setOffset($offset);
        }

        return $this;
    }

    public function setOffset($offset)
    {
        $this->offset   = (int)$offset;

        return $this;
    }

    public function setOrderBy($column, $direction = null)
    {
        $this->orderColumn  = $column;

        if (!is_null($direction))
        {
            $this->setOrderDirection($direction);
        }

        return $this;
    }

    public function setOrderDirection($direction)
    {
        $this->orderDirection   = strtoupper($direction);

        return $this;
    }

    public function guessEntityName()
    {
        $segments   = explode('.', $this->from);

        return ucfirst(trim($segments[count($segments) - 1], '`'));
    }


    public function __toString()
    {
        $columns    = array();

        foreach ($this->getColumns() as $alias => $column)
        {
            $columns[]  = is_int($alias) ? $column : $column . ' AS ' . $alias;
        }

        if (is_null($this->query))
        {
            $this->query    = str_replace('{COLUMNS}', implode(', ', $columns), self::$syntax[$this->type]);
        }

        $query  = str_replace('{MODIFIERS}', implode(' ', array_keys($this->modifiers)), $this->query);
        $query  = str_replace('{FROM}', $this->from, $query);

        $limit  = is_null($this->limit) ? '' : 'LIMIT ' . $this->limit . ' OFFSET ' . $this->offset;

        $query  = str_replace('{LIMIT}', $limit, $query);

        $orderBy    = is_null($this->orderColumn) ? '' : 'ORDER BY `' . $this->orderColumn . '` ' . $this->orderDirection;

        $query  = str_replace('{ORDER_BY}', $orderBy, $query);

        $where  = is_null($this->where) ? '' : 'WHERE ' . $this->where;

        $query  = str_replace('{WHERE}', $where, $query);

        return $query;
    }
}
