<?php

class TwStringsTool
{
    protected static $downgradeUtf8Search     = array(
        'Ț', 'ț',
        'Ș', 'ș',
        'Ă', 'ă',
        'Â', 'â',
        'Î', 'î',
    );

    protected static $downgradeUtf8Replace    = array(
        'T', 't',
        'S', 's',
        'A', 'a',
        'A', 'a',
        'I', 'i',
    );

    static function downgradeUtf8($text)
    {
        return str_replace(self::$downgradeUtf8Search, self::$downgradeUtf8Replace, $text);
    }

    static function camelCasedToSnakeCased($text)
    {
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $text, $matches);

        $ret    = $matches[0];

        foreach ($ret as &$match)
        {
            $match  = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
        }

        return implode('_', $ret);
    }

    static function snakeCasedToCamelCased($text, $ucFirst = FALSE)
    {
        $text   = str_replace(' ', '', ucwords(str_replace('_', ' ', $text)));

        if ($ucFirst){
            return $text;
        }

        return strtolower(substr($text,0,1)).substr($text,1);
    }

    static function getExcerpt($text, $maximumLenght, $suffix = ' [...]')
    {
        if (strlen($text) > $maximumLenght)
        {
            $excerpt    = substr($text, 0, $maximumLenght - 3);
            $lastSpace  = strrpos($excerpt, ' ');
            $excerpt    = substr($excerpt, 0, $lastSpace);
            $excerpt    .= $suffix;
        } else
        {
            $excerpt    = $text;
        }

        return $excerpt;
    }
}
