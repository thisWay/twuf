<?php

class TwTime
{
    static function getTimeStamp()
    {
        return time();
    }

	static function getDateTime()
	{
		return self::getDate('Y-m-d H:i:s');
	}

    static function getDateTimeMs()
    {
        $microTime      = microtime(true);
        $microSeconds   = substr($microTime, strpos($microTime, '.') + 1);

        return self::getDate('Y-m-d H:i:s') . '.' . $microSeconds;
    }

	static function getDate($format = 'Y-m-d')
	{
        return date($format);
	}

    static function addToDateTime($interval, $dateTime, $outputFormat = 'Y-m-d H:i:s')
    {
        return date($outputFormat, strtotime($interval, strtotime($dateTime)));
    }

	static function getDateTimeFromYmdHis($YmdHis)
	{
		$year	= substr($YmdHis, 0, 4);
		$month	= substr($YmdHis, 4, 2);
		$day	= substr($YmdHis, 6, 2);
		$hour	= substr($YmdHis, 8, 2);
		$minute	= substr($YmdHis, 10, 2);
		$second	= substr($YmdHis, 12, 2);

		return "{$year}-{$month}-{$day} {$hour}:{$minute}:{$second}";
	}
}