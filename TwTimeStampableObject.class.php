<?php

/**
 * Class TwTimeStampableObject
 *
 * @method  setCreatedAt($createdAt)
 * @method  setUpdatedAt($createdAt)
 * @method  setDeletedAt($createdAt)
 *
 * @method  getCreatedAt()
 * @method  getUpdatedAt()
 * @method  getDeletedAt()
 */
abstract class TwTimeStampableObject extends TwObject
{
    protected $timeStampable = array(
        'createdAt'     => null,
        'modifiedAt'    => null,
        'updatedAt'     => null,
        'deletedAt'     => null,
    );

    public function __construct($attributes = null)
    {
        $this->onCreate();

        $this->init();

        parent::__construct($attributes);
    }

    protected function onCreate()
    {
        $this->timeStampable['createdAt']   = TwTime::getDateTimeMs();

        return $this;
    }

    public function init(){}






    protected function onUpdate()
    {
        $this->__updatedAt	= TwTime::getDateTimeMs();

        return $this;
    }

    public function onModify()
    {
        $this->__modifiedAt = TwTime::getDateTimeMs();

        return $this;
    }



    protected function onDelete()
    {
        $this->__deletedAt	= TwTime::getDateTimeMs();

        return $this;
    }

    public function getTsCreatedAt()
    {
        return $this->__createdAt;
    }

    public function getTsModifiedAt()
    {
        return $this->__modifiedAt;
    }

    public function getTsUpdatedAt()
    {
        return $this->__updatedAt;
    }

    public function getTsDeletedAt()
    {
        return $this->__deletedAt;
    }
}
