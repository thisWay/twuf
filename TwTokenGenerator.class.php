<?php

class TwTokenGenerator
{
	static function getToken($lenght = 10)
	{
		return substr(bin2hex(openssl_random_pseudo_bytes((int)(($lenght + 1) / 2))), 0, $lenght);
	}
}