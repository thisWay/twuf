<?php

/**
 * Class TwWordPressGMedia
 *
 * @method setId(integer $id)
 * @method setAuthorId(integer $authorId)
 * @method setCreatedAt(string $createdAt)
 * @method setDescription(string $description)
 * @method setTitle(string $title)
 * @method setGmuid(string $gmuid)
 * @method setLink(string $link)
 * @method setModifiedAt(string $modifiedAt)
 * @method setMimeType(string $mimeType)
 * @method setStatus(string $status)
 *
 * @method getId()
 * @method getName()
 * @method getSlug()
 * @method getGroup()
 */
class TwWordPressGMedia extends TwObject
{
    protected $id;
    protected $authorId;
    protected $createdAt;
    protected $description;
    protected $title;
    protected $gmuid;
    protected $link;
    protected $modifiedAt;
    protected $mimeType;
    protected $status;

    static $mapping = array(
        'ID'            => 'id',
        'author'        => 'authorId',
        'date'          => 'createdAt',
        'description'   => 'description',
        'title'         => 'title',
        'gmuid'         => 'gmuid',
        'link'          => 'link',
        'modified'      => 'modifiedAt',
        'mime_type'     => 'mimeType',
        'status'        => 'status',
    );
}
