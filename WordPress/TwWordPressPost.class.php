<?php

/**
 * Class TwWordPressPost
 *
 * @method setId(integer $id)
 * @method setTitle(string $title)
 * @method setContent(string $content)
 * @method setAuthor(string $author)
 * @method setDate(string $date)
 * @method setStatus(string $status)
 * @method setName(string $name)
 * @method setType(string $type)
 *
 * @method getId()
 * @method getTitle()
 * @method getContent()
 * @method getAuthor()
 * @method getDate()
 * @method getStatus()
 * @method getName()
 * @method getType()
 *
 */
class TwWordPressPost extends TwObject
{
    protected $id;
    protected $author;
    protected $date;
    protected $content;
    protected $title;
    protected $status;
    protected $name;
    protected $type;

    static $mapping = array(
//        'id'            => 'id',
        'ID'            => 'id',
        'post_author'   => 'author',
        'post_date'     => 'date',
        'post_content'  => 'content',
        'post_title'    => 'title',
        'post_status'   => 'status',
        'post_name'     => 'name',
        'post_type'     => 'type',
    );


}