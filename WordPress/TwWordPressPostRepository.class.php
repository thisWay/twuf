<?php

class TwWordPressPostRepository extends TwMySqlRepository
{
    protected $tableName    = 'wp_posts';


    public function add(TwWordPressPost $post)
    {
        return parent::add($post);
    }

    public function insert(TwWordPressPost $post)
    {
        print_r($post);

        $sql    = "INSERT INTO {$this->getContainer()} SET
            `post_title`    = '{$post->getTitle()}',
            `post_content`  = '{$post->getContent()}',
            `post_name`     = '{$post->getName()}',
            `post_author`   = '{$post->getAuthor()}',
            `post_type`     = '{$post->getType()}',
            `post_status`   = '{$post->getStatus()}',
            `post_date`     = '{$post->getDate()}'
        ;";

        $this->getMySqlPdo()->run($sql);
    }
}