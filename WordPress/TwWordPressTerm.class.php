<?php

/**
 * Class TwWordPressTerm
 *
 * @method setId(integer $id)
 * @method setName(string $name)
 * @method setSlug(string $slug)
 * @method setGroup(integer $group)
 *
 * @method getId()
 * @method getName()
 * @method getSlug()
 * @method getGroup()
 */
class TwWordPressTerm extends TwObject
{
    protected $id;
    protected $name;
    protected $slug;
    protected $group;

    static $mapping = array(
        'term_id'       => 'id',
        'name'          => 'name',
        'slug'          => 'slug',
        'term_group'    => 'group',
    );
}
