<?php

/**
 * Class TwWordPressTermRelationship
 *
 * @method  getObjectId()
 * @method  getTermTaxonomyId()
 *
 * @method  setObjectId(integer $id)
 * @method  setTermTaxonomyId(integer $id)
 */
class TwWordPressTermRelationship extends TwObject
{
    protected $objectId;
    protected $termTaxonomyId;

    static $mapping = array(
        'object_id'         => 'objectId',
        'term_taxonomy_id'  => 'termTaxonomyId',
    );
}