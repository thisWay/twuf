<?php

/**
 * Class TwWordPressTermRelationshipRepository
 */
class TwWordPressTermRelationshipRepository extends TwMySqlRepository
{
    protected $tableName    = 'wp_term_relationships';

    public function findOneByObjectIdAndTermTaxonomyId($objectId, $termTaxonomyId)
    {
        $sql    = 'SELECT * FROM ' . $this->getContainer() . ' WHERE `object_id` = :object_id AND `term_taxonomy_id` = :term_taxonomy_id;';

        return $this->getMySqlPdo()->fetchOneAsObject($sql, array(':object_id' => $objectId, ':term_taxonomy_id' => $termTaxonomyId), 'TwWordPressTermRelationship');
    }


    public function add(TwWordPressTermRelationship $termRelationship)
    {
        return parent::add($termRelationship);
    }
}