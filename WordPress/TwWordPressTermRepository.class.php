<?php

/**
 * Class TwWordPressTermRepository
 */
class TwWordPressTermRepository extends TwMySqlRepository
{
    protected $tableName    = 'wp_terms';

    /**
     * @param $name
     * @return TwWordPressTerm
     */
    public function findOneByName($name)
    {
        $sql    = 'SELECT * FROM ' . $this->getContainer() . ' WHERE `name` = :name;';

        return $this->getMySqlPdo()->fetchOneAsObject($sql, array(':name' => $name), 'TwWordPressTerm');
    }

    public function add(TwWordPressTerm $term)
    {
        return parent::add($term);
    }
}