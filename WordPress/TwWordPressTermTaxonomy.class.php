<?php

/**
 * Class TwWordPressTermTaxonomy
 *
 * @method  getId()
 * @method  getTermId()
 * @method  getTaxonomy()
 * @method  getCount()
 *
 * @method  setId(integer $id)
 * @method  setTermId(integer $id)
 * @method  setTaxonomy(string $taxonomy)
 * @method  setCount(integer $count)
 */
class TwWordPressTermTaxonomy extends TwObject
{
    protected $id;
    protected $termId;
    protected $taxonomy;
    protected $count;

    static $mapping = array(
        'term_taxonomy_id'  => 'id',
        'term_id'           => 'termId',
        'taxonomy'          => 'taxonomy',
        'count'             => 'count',
    );
}