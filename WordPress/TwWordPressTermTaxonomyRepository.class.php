<?php

/**
 * Class TwWordPressTermTaxonomyRepository
 */
class TwWordPressTermTaxonomyRepository extends TwMySqlRepository
{
    protected $tableName    = 'wp_term_taxonomy';
    protected $primaryKey   = array('term_taxonomy_id');

    /**
     * @param $termId
     * @param $taxonomy
     * @return bool|TwWordPressTermTaxonomy
     */
    public function findOneByTermIdAndTaxonomy($termId, $taxonomy)
    {
        $sql    = 'SELECT * FROM ' . $this->getContainer() . ' WHERE `term_id` = :term_id AND `taxonomy` = :taxonomy;';

        return $this->getMySqlPdo()->fetchOneAsObject($sql, array(':term_id' => $termId, ':taxonomy' => $taxonomy), 'TwWordPressTermTaxonomy');
    }


    public function add(TwWordPressTermTaxonomy $termTaxonomy)
    {
        return parent::add($termTaxonomy);
    }

    public function save(TwWordPressTermTaxonomy $termTaxonomy)
    {
        return parent::save($termTaxonomy);
    }
}