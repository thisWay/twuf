<?php

/**
 * Class TwWordPressUser
 *
 * @method setId(integer $id)
 * @method setLogin(string $login)
 * @method setPassword(string $password)
 * @method setNiceName(string $niceName)
 * @method setEmail(string $email)
 * @method setRegisteredAt(string $registeredAt)
 * @method setDisplayName(string $displayName)
 *
 * @method getId()
 * @method getLogin()
 * @method getPassword()
 * @method getNiceName()
 * @method getEmail()
 * @method getRegisteredAt()
 * @method geDisplayName()
 */
class TwWordPressUser extends TwObject
{
    protected $id;
    protected $login;
    protected $password;
    protected $niceName;
    protected $email;
    protected $registeredAt;
    protected $displayName;

    static $mapping = array(
//        'id'            => 'id',
        'ID'                => 'id',
        'user_login'        => 'login',
        'user_pass'         => 'password',
        'user_nicename'     => 'niceName',
        'display_name'      => 'displayName',
        'user_email'        => 'email',
        'user_registered'   => 'registeredAt',
    );


}