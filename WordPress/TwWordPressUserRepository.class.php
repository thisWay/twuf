<?php

class TwWordPressUserRepository extends TwMySqlRepository
{
    protected $tableName    = 'wp_users';

    /**
     * @param $userLogin
     * @return bool|TwWordPressUser
     */
    public function findOneByUserLogin($userLogin)
    {
        $sql    = 'SELECT * FROM ' . $this->getContainer() . ' WHERE `user_login` = :user_login;';

        return $this->getMySqlPdo()->fetchOneAsObject($sql, array(':user_login' => $userLogin), 'TwWordPressUser');
    }

    public function add(TwWordPressUser $user)
    {
        return parent::add($user);
    }

/*    public function insert(TwWordPressUser $user)
    {
        print_r($post);

        $sql    = "INSERT INTO {$this->getContainer()} SET
            `post_title`    = '{$post->getTitle()}',
            `post_content`  = '{$post->getContent()}',
            `post_name`     = '{$post->getName()}',
            `post_author`   = '{$post->getAuthor()}',
            `post_type`     = '{$post->getType()}',
            `post_status`   = '{$post->getStatus()}',
            `post_date`     = '{$post->getDate()}'
        ;";

        $this->getMySqlPdo()->run($sql);
    }
*/
}